var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var SiteSchema = new Schema({
	   	'title': { type: String, index: true, required: true },
		'domain': { type: String, index: { unique: true }, required: true, trim: true },
		'description': { type: String },
		'keywords': { type: String },
		'data':{ type: String},//  Iam not sure	
		'created': { type: Date, default: Date.now, required: true }
});


module.exports = mongoose.model('Site', SiteSchema);