var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var PageSchema = new Schema({
	'title': { type: String, index: true, required: true },
	'description': { type: String },
	'keywords': { type: String },
	'author': { type: Schema.ObjectId, ref: 'User', required: true, index: true },
	'status': { type: String, required: true, enum: ['public', 'private', 'share'], index: true },
	'created': { type: Date, default: Date.now, required: true }
});


module.exports = mongoose.model('Page', PageSchema);