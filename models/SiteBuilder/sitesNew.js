var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var SiteSchema = new Schema({
	   	'title': { type: String, index: true, required: true },
		'domain': { type: String, index: { unique: true }, required: true, trim: true },
		'description': { type: String },
		'keywords': { type: String },
		'pages':[{ type: Schema.ObjectId, ref: 'Page', index: true }],//Iam not sure
		'groups':[{ type: Schema.ObjectId, ref: 'Group', index: true }],
		'author': { type: Schema.ObjectId, ref: 'User', required: true, index: true },
		'status': { type: String, required: true, enum: ['public', 'private', 'share'], index: true },
		'created': { type: Date, default: Date.now, required: true }
});


module.exports = mongoose.model('Site', SiteSchema);