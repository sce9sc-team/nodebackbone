var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var RoleSchema = new Schema({
	   	'name': { type: String, index: true, required: true },
		'description': { type: String },
		'permissions':[{ type: String, required: true, enum: ['create','read','update','delete'], index: true }],
		'app':{ type: String, required: true, index: true },
		'createdBy': { type: Schema.ObjectId, ref: 'User', required: true, index: true },
		'created': { type: Date, default: Date.now, required: true }
});


module.exports = mongoose.model('Role', RoleSchema);