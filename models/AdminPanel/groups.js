var mongoose = require('mongoose'),
    Schema = mongoose.Schema;


var GroupSchema = new Schema({
	    'name': { type: String, index: true, required: true },	
		'roles':[{type: Schema.ObjectId,ref: 'Role'}],
		'description': { type: String },
		'created': { type: Date, default: Date.now, required: true }
});  

module.exports = mongoose.model('Group', GroupSchema);