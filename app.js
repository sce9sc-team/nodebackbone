var express = require('express.io')
	, path = require('path')
	, mongoose = require('mongoose')
	, redis = require('redis')
	, RedisStore = express.io.RedisStore
	, RedisStoreEx = require('connect-redis')(express)
	, settings = require('./settings/settings');



	// This is what the workers will do.
workers = function() {
	//console.log(settings[0].name);	
	var app = express();
	app.http().io();


	app.io.configure(function(){
		app.io.set('transports', ['websocket']);
	});

/*
	The methodOverride middleware allows Express apps to behave like RESTful apps
	, as popularised by Rails; HTTP methods like PUT can be used through hidden inputs. 
*/
	//var store = new RedisStoreEx
	app.configure(function(){
	  //app.set('port', process.env.PORT || 3000);
		app.set('views', __dirname + '/views');
		app.set('view engine', 'jade');	
		app.use(express.favicon());
		app.use(express.cookieParser());
		app.use(express.session({store: new RedisStoreEx,
				secret: 'monkey',
				cookie:{ maxAge:null}
				}));
		//app.use(passport.initialize());
	 	//app.use(passport.session());
	  	//app.use(express.logger('dev'));
	  	app.use(express.bodyParser());
	  	//app.use(express.methodOverride());
	  	app.use(app.router);
	  	app.use(express.static(path.join(__dirname, 'public')));
	});

	app.io.set('store', new express.io.RedisStore({
	        redisPub: redis.createClient(),
	        redisSub: redis.createClient(),
	        redisClient: redis.createClient()
	    }))
	app.io.enable('browser client gzip');


	mongoose.connect('mongodb://localhost/backbone',function(err) {
	    if (err) throw err;
	    console.log('Successfully connected to MongoDB');
	});

	// ----------Setup routes for socket.io and express, --------------
	settings.startApps(app);
	//--------------End Setup routes  ------------------------------------

	
	//configuration 
	app.get('/api/test', function(req, res){
		settings.createDb();
	   	var todo = [{el:'test1',html:'111111111'},{el:'test2',html:'22222222222'}];
	   	var ret = JSON.stringify(todo);
	   	res.setHeader('Content-Type', 'application/json');
	   	res.setHeader('Accept', 'application/json');
	   	return res.send(ret);
  
	});
	//End configuration -------------------------------

	app.listen(3000)

}


// Start forking if you are the master.
cluster = require('cluster')
numCPUs = require('os').cpus().length;

if (cluster.isMaster) {
    for (var i = 0; i < numCPUs; i++) { cluster.fork() }
} else { workers() }

