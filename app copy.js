
/**
 * Module dependencies.
 */

var express = require('express')
  , application_root = __dirname
  , routes = require('./routes')
  , user = require('./routes/user')
  , http = require('http')
  , path = require('path')
  , mongoose = require('mongoose');
	User = require('./models/users');
	
	
var app = express();


mongoose.connect('mongodb://localhost/backbone',function(err) {
    if (err) throw err;
    console.log('Successfully connected to MongoDB');
});




app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});
	
app.configure('development', function(){
  app.use(express.errorHandler());
});

app.get('/', routes.index);
//app.get('/users', user.list);
app.get('/api/test', function(req, res){
   var todo = [{el:'test1',html:'111111111'},{el:'test2',html:'22222222222'}];
   var ret = JSON.stringify(todo);
   res.setHeader('Content-Type', 'application/json');
   res.setHeader('Accept', 'application/json');
   return res.send(ret);
  
});


var server = http.createServer(app);
var io = require('socket.io').listen(server);
io.sockets.on('connection', function (socket) {
  socket.emit('news', { hello: 'world' });
  socket.on('my other event', function (data) {
    console.log(data);
  });
  
  socket.on('getPages', function (data) {
	
	var todo = [{el:'test5',html:'555555'},{el:'test6',html:'6666666666'}];
    socket.emit('pages', {pages:todo});
	
	
  });
 socket.on('setPages', function (data) {

	var todo = [{el:'test8',html:'888888'},{el:'test9',html:'9999999999'}];
    socket.broadcast.emit('pages', {pages:todo});


  });

	var todo = [{el:'home',html:'This is home'},{el:'test4',html:'44444444'}];
 	//var ret = JSON.stringify(todo);
	console.log(todo);
  	socket.emit('pages',{pages:todo})
  

});




server.listen(3000);
/*
http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
*/