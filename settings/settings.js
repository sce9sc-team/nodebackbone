var Step = require('step');

var apps = [{
				name:'AdminPanel',
				routes:[{path:'/adminPanel',route:'index'}],
				routesio:[
					'ready',
					'login',
					'logout',
					'register',
					'isloggedin',
					'testemit',
					'saveSite',
					'savePage',
				],
				views:'',
				models:['groups','roles','users'],	
			},
			{
				name:'SiteBuilder',
				routes:[{path:'/',route:'index'}],
				routesio:[],
				views:'',
				//models:['pages','sites'],	
				models:['sites'],	
				
			}];

function startApps(app)
{
	console.log('configuring apps');
	
	for(var d=0;d<apps.length;d++)
	{
	
		var AppName = apps[d].name;
		console.log('configuring app : '+AppName);
		var routesIo = apps[d].routesio;
		var routes = apps[d].routes;
		var models = apps[d].models;
		var AppRoutes = require('../routes/'+AppName);
	
		for(var i=0;i<routesIo.length;i++)
		{
			require('../routesio/'+AppName+'/'+routesIo[i])[routesIo[i]](app);
		}
		for(var i=0;i<routes.length;i++)
		{
			app.get(routes[i].path, AppRoutes[routes[i].route]);
		}	
		for(var i=0;i<models.length;i++)
		{
			require('../models/'+AppName+'/'+models[i]);
		}	
	}

};
exports.startApps = startApps; 


function createDb(){
	var User = require('../models/AdminPanel/users');
	var Group = require('../models/AdminPanel/groups');
	var Role = require('../models/AdminPanel/roles');
	
	
	// create users
	var initApp = 'AdminPanel';
	
	var initUsers = [{name:'admin',email:'sce9sc@hotmail.com',username:'admin',password:'iw2fure,.'},
				 	{name:'test',email:'sce9sc1@hotmail.com',username:'test',password:'iw2fure,.'}];
	
	
	var initRoles=	[{'name': 'Admin',
					'description': 'Admin role for AdminPanel',
					'permissions':['create','read','update','delete'],
					'app':'AdminPanel'},
					{'name': 'User',
					 'description': 'User role for AdminPanel',
					 'permissions':['read','update'],
					 }];
	

	
	function createUsers()
	{
		Step(
			function(){
				console.log('Starting creating Users');
				var group = this.group();
				for(var i=0;i<initUsers.length;i++)
				{
					var myuser = new User(initUsers[i]);	
					myuser.save(group());
				}
			},
			function (err,res)
			{
				if (err) throw err;
				
				console.log('Users -- created');
				createRoles();
				
			}
			);
	};
	
	function createRoles(){
		User.findOne({name:'admin'},function(err,adminUser){
			console.log(adminUser);
			Step(
				function(){
					console.log('Starting creating Roles');
					var group = this.group();
					for(var i=0;i<initRoles.length;i++)
					{
						initRoles[i].createdBy=adminUser._id;
						initRoles[i].app =initApp;
						var newrole = new Role(initRoles[i]);						
						newrole.save(group());
					}
				},function(err,res){
					if (err) throw err;
					console.log('Roles -- created');
					console.log(res);
					createGroups(res)
				});				
		});
	};

	function createGroups(roles){
		
		Step(
			function(){
				var group = this.group();
				for(var i=0;i<roles.length;i++)
				{
					if(roles[i].name=="Admin")
					{
						var initgroups = {name:'Admins',roles:[roles[i]]};					
					}else
					{
						var initgroups = {name:'Users',roles:[roles[i]]};		
					}
					var newgroup = new Group(initgroups);
					newgroup.save(group());
				
				}
			},
			function(err,grps){				
				User.findOne({name:'admin'},function(err,usr){
						for(var i=0;i<grps.length;i++)
						{
							if(grps[i].name=="Admins")
							{
								usr.memberOf.push(grps[i]._id);
								usr.save();
							}
						}
				});
				
				console.log(grps)
			});
	}
	
	createUsers();
	
};

exports.createDb = createDb; 

