define([
		'zepto',
		'underscore',
		'backboneAssociations',
		'models/widget',
		'views/widgetView',
		],
function($,_, Backbone,WidgetEng)
{
	WidgetEng.SiteView = WidgetEng.WidgetView.extend({
		events: {
		"click":"setCurrentWidget",
		"mousemove":"moveResizeEvent",
		"mousedown":"startResizeEvent",
		"mouseup .site":"stopResizeEvent",
		"mouseover .site":"hoverElements",
		},
		editable:function(e){
				e.target.setAttribute("contenteditable","true");
				e.target.focus();
		},
		getCurrentSelectedWidget:function(w){
			console.log('-------')
			console.log(w)
			var res = null;
			function find(wgt){
				if(wgt.dataset.selectable){
					if(wgt.dataset.selectable=="false"){
						find(wgt.parentNode)
					}else{
					  res=wgt;
					}
				}
				else{find(wgt.parentNode)}		
			}
			find(w);
			return res
		},
		setCurrentWidget:function(e)
		{
			var w = this.getCurrentSelectedWidget(e.target);

			if(e.target.className != 'handles'){
			if(w.id == this.el.id)
			{
					if(this.model.get('selectable'))
					{
						if(this.model.get('currentSelectedWidget')!=this.model)
						{
							var currentWidgt = this.model.get('currentSelectedWidget');
							this.model.set({'currentSelectedWidget':this.model});
							if(currentWidgt.get('handles'))
							{
								currentWidgt.set({handles:false});
							}							
						}else
						{
							this.model.set({'currentSelectedWidget':this.model});
						}
					}
			}
			else
			{
				var wigt = this.model.getWidgetById(w.id)[0];
				if(wigt.get('selectable'))
				{
						if(this.model.get('currentSelectedWidget')==wigt)
						{
							//unset
							this.model.set({'currentSelectedWidget':this.model});
							wigt.set({handles:false});
						}else{
							//set
							if(this.model.get('currentSelectedWidget'))
							{
								var curWigt = this.model.get('currentSelectedWidget');
								curWigt.set({handles:false});
							}
							this.model.set({'currentSelectedWidget':wigt});
							wigt.set({handles:true});

						}

				}
			}}
		},
		moveResizeEvent:function(e)
		{	
			var targetWidget = this.model.get('currentSelectedWidget');
			if(targetWidget){
				if(targetWidget.get('canResize'))
				{
					switch(this.options.resizedir)
					{
						case 'w':
							var parentWidth = parseInt(window.getComputedStyle(targetWidget.get('parent').view.el,null).getPropertyValue("width").replace("px",''));
							var width = parseInt(targetWidget.get('width').replace("px",''));			
							var newWidth = width+(e.pageX-this.options.pageX)
						//	if(newWidth<=parentWidth){				
								targetWidget.set({width:newWidth+'px'});
								this.options.pageX = e.pageX;					
						//	}	
							break;
						case 'h':
							var parentHeight= parseInt(window.getComputedStyle(targetWidget.get('parent').view.el,null).getPropertyValue("height").replace("px",''));
							var height = parseInt(targetWidget.get('height').replace("px",''));			
							var newHeight = height+(e.pageY-this.options.pageY)
							//if(newHeight<=parentHeight){				
								targetWidget.set({height:newHeight+'px'});
								this.options.pageY = e.pageY;					
							//}
							break;
					}	
				}
			}
		},
		hoverElements:function(e)
		{
			//show widgets you are on top
		},
		startResizeEvent:function(e)
		{
			if(e.target.className == 'handles'){
				this.options.pageX = e.pageX;
				this.options.pageY = e.pageY;
				this.model.get('currentSelectedWidget').set({'canResize':true})

				switch(e.target.id)
				{
					case 'tst':
						this.options.resizedir ='w';
						break;
					case 's1':
						this.options.resizedir ='h';
						break;
				 	default:
						this.options.resizedir ='h';
						break;
				}
			}

		},	
		stopResizeEvent:function(e)
		{
			if(this.model.get('currentSelectedWidget')){
			if(this.model.get('currentSelectedWidget').get('canResize'))
			{
				this.model.get('currentSelectedWidget').set({'canResize':false})
			}}
		},

	});
	
	return WidgetEng;
	
	
});