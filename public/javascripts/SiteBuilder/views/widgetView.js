define([
		'zepto',
		'underscore',
		'backboneAssociations',
		'models/widget'
		],
function($,_, Backbone,WidgetEng)
{
	WidgetEng.WidgetView = Backbone.View.extend({	

			initialize: function(){
			  		console.log('initializing Widget');	
					if(this.model.get('parent'))
					{
						$('#'+this.model.get('parent').get('el')).append(this.el); //append this controller el to the page el 					
					}	          
		            this.listenTo(this.model, "remove", this.remove); //remove the view when object is removed        

		            this.listenTo(this.model, "change:el", this.updateEl); //update el when the objects el has changed
					this.listenTo(this.model, "change:background", function(){this.el.style.background = this.model.get('background') });
					this.listenTo(this.model, "change:width", function(){this.el.style.width = this.model.get('width') });
					this.listenTo(this.model, "change:height", function(){this.el.style.height = this.model.get('height') });
					this.listenTo(this.model, "change:border", function(){this.el.style.border = this.model.get('border') });
					this.listenTo(this.model, "change:color", function(){this.el.style.color = this.model.get('color') });
					this.listenTo(this.model, "change:events", this.eventsChange);

					//this.listenTo(this.model, "change:events", function(model){
					//	console.log(model.get('events'))
					//});
					this.render();
		    },
			render: function()
			{
				console.log(this.$el);	
				this.el.className = this.model.get('classname');
				this.el.style.width = this.model.get('width');
				this.el.style.height = this.model.get('height');
				this.el.style.border = this.model.get('border');
				this.el.style.background = this.model.get('background') ||"white";
				this.el.dataset.selectable = "true";
				this.el.dataset.widget = this.model.get('classname');
				return this; //recommended as this enables calls to be chained.
			}, 
			updateEl:function(widget)
			{
				console.log('PageController updateEl event triggered');
				this.$el.attr('id',widget.get('el'))		
			},
			eventsChange:function(wgt)
			{
				var prevEvents = wgt.get('prevEvents');
				for(var i=0;i<prevEvents.length;i++)
				{
					delete this[prevEvents[i][_.keys(prevEvents[i])[0]]];
				}

				this.undelegateEvents();
				var events = {};
				for(var i=0;i<wgt.changed.events.length;i++)
				{
					var eventName = _.values(wgt.changed.events[i])[0];
					var eventkey = _.keys(wgt.changed.events[i])[0];
					this[eventName] = WidgetEng.customEvents[eventName];
					events[eventkey] = eventName;
				}
				this.delegateEvents(events);
			},
			fireCustomEvent:function(eventName)
			{
				var eventsArray = Object.keys(WidgetEng.customEvents);
				if(eventsArray.indexOf(eventName)>-1)
				{
					WidgetEng.customEvents[eventName]();
				}
			}

		});
	
	return WidgetEng;
	
});