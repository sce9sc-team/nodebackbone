define([
		'zepto',
		'underscore',
		'backboneAssociations',
		'models/widget',
		'views/widgetView',
		],
function($,_, Backbone,WidgetEng)
{

	WidgetEng.BoxView = WidgetEng.WidgetView.extend({

		render: function()
		{
			console.log(this.$el);	
			this.el.className = this.model.get('classname');
			this.el.style.width = this.model.get('width');
			this.el.style.height = this.model.get('height');
			this.el.style.background = this.model.get('background');
			this.el.style.border = this.model.get('border');
			this.el.style.position ="relative";
			this.el.style.overflow = 'hidden';
			this.el.style.boxSizing="border-box";
			this.el.style.MozBoxSizing="border-box";
			this.el.style.webkitBoxSizing="border-box";
			this.el.dataset.widget = this.model.get('classname');
			this.addHandles();
			this.postRender();
		
			return this; //recommended as this enables calls to be chained.
		
				
		},
		postRender:function()
		{
			this.el.dataset.selectable = this.model.get('selectable');
			this.listenTo(this.model, "change:selectable", function(){this.el.dataset.selectable = this.model.get('selectable') });
		},
		showHandles:function(){
			for(var i=0;i<this.handles.length;i++)
			{
				this.handles[i].style.display = "block";
			}
		},
		hideHandles:function(){
			for(var i=0;i<this.handles.length;i++)
			{
				this.handles[i].style.display = "none";
			}
		},
		addHandles:function()
		{
			this.handles =[];
			var fragment = document.createDocumentFragment();
			var b1 = document.createElement('div');
			b1.className="handles";
			b1.style.cssText = 'z-index:98;display:none;position:absolute;left:100%;top:0px;width:1px; margin-left:-1px; height:100%; border:1px dashed yellow;box-sizing: border-box; ';
			b1.dataset.selectable = "false";
			this.handles.push(b1);
		
			var b2 = document.createElement('div')
			b2.className="handles"
			b2.style.cssText = 'z-index:98;display:none;position:absolute;left:-1px;top:0px;width:1px; height:100%; border:1px dashed yellow;box-sizing: border-box; ';
			b2.dataset.selectable = "false";
			this.handles.push(b2);
		
			var b3 = document.createElement('div')
			b3.className="handles"
			b3.style.cssText = 'z-index:98;display:none;position:absolute;top:-1px;height:1px; width:100%; border:1px dashed yellow;box-sizing: border-box; ';
			b3.dataset.selectable = "false";
			this.handles.push(b3);
		
			var b4 = document.createElement('div')
			b4.className="handles"
			b4.style.cssText = 'z-index:98;display:none;position:absolute;bottom:-1px;width:100%; height:1%; border:1px dashed yellow;box-sizing: border-box; ';
			b4.dataset.selectable = "false";
			this.handles.push(b4);
		
			var a1 = document.createElement('div')
			a1.className="handles"
			a1.style.cssText = 'z-index:99;display:none;position:absolute;top:-1px;left:-1px;width: 10px; height: 10px; background-color: white; border: 1px solid black; box-sizing: border-box; ';
			a1.dataset.selectable = "false";
			this.handles.push(a1);
		
			var a2 = document.createElement('div')
			a2.className="handles"
			a2.style.cssText = 'z-index:99;display:none;position:absolute;top:-1px;margin-left:-5px;left:50%;width: 10px; height: 10px; background-color: white; border: 1px solid black; box-sizing: border-box; ';
			a2.dataset.selectable = "false";
			this.handles.push(a2);
		
			var a3 = document.createElement('div')
			a3.className="handles"
			a3.style.cssText = 'z-index:99;display:none;position:absolute;top:-1px;right:-1px;width: 10px; height: 10px; background-color: white; border: 1px solid black; box-sizing: border-box; ';
			a3.dataset.selectable = "false";
			this.handles.push(a3);
		
			var a4 = document.createElement('div')
			a4.className="handles"
			a4.id="tst"
			a4.style.cssText = 'z-index:99;display:none;position:absolute;margin-top:-5px;top:50%;right:-1px;width: 10px; height: 10px; background-color: white; border: 1px solid black; box-sizing: border-box; ';
			a4.dataset.selectable = "false";
			this.handles.push(a4);
		
			var a5 = document.createElement('div')
			a5.className="handles"
			a5.style.cssText = 'z-index:99;display:none;position:absolute;bottom:-1px;left:-1px;width: 10px; height: 10px; background-color: blue; border: 1px solid black; box-sizing: border-box; ';
			a5.dataset.selectable = "false";
			this.handles.push(a5);
		
			var a6 = document.createElement('div')
			a6.className="handles"
			a6.style.cssText = 'z-index:99;display:none;position:absolute;top:50%;margin-top:-5px;left:-1px;width: 10px; height: 10px; background-color: blue; border: 1px solid black; box-sizing: border-box; ';
			a6.dataset.selectable = "false";
			this.handles.push(a6);
		
			var a7 = document.createElement('div')
			a7.className="handles"
			a7.id="s1"
			a7.style.cssText = 'z-index:99;display:none;position:absolute;bottom:-1px;margin-left:-5px;left:50%;width: 10px; height: 10px; background-color: white; border: 1px solid black; box-sizing: border-box; ';
			a7.dataset.selectable = "false";
			this.handles.push(a7);
			
			var a8 = document.createElement('div')
			a8.className="handles"
			a8.style.cssText = 'z-index:99;display:none;position:absolute;bottom:-1px;right:-1px;width: 10px; height: 10px; background-color: blue; border: 1px solid black; box-sizing: border-box; ';
			a8.dataset.selectable = "false";
			this.handles.push(a8);	
		
			fragment.appendChild(b1);
			fragment.appendChild(b2);
			fragment.appendChild(b3);
			fragment.appendChild(b4);
			fragment.appendChild(a1);
			fragment.appendChild(a2);
			fragment.appendChild(a3);
			fragment.appendChild(a4);
			fragment.appendChild(a5);
			fragment.appendChild(a6);
			fragment.appendChild(a7);
			fragment.appendChild(a8);
		
			this.el.appendChild(fragment);
		
			this.listenTo(this.model, "change:handles", function(){
				if(this.model.get('handles'))
				{
					this.showHandles();
				}else
				{
					this.hideHandles();
				}
			});
		
	
		
		}
	});
	
	return WidgetEng;
	
});