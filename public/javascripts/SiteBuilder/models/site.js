define([
		'zepto',
		'underscore',
		'backboneAssociations',
		'models/widget'],
function($,_, Backbone,WidgetEng)
{
	WidgetEng.Site = WidgetEng.Widget.extend({
		defaults:_.extend({},WidgetEng.Widget.prototype.defaults,{
			classname:"site",
			el:"site",
			viewClass: "SiteView"
		}),
		addInitialView:function(){
			if(this.get('el')=='site'){	
				this.view = new WidgetEng[this.get("viewClass")]({el:$("#"+this.get('el')),model:this});			
			}	
		},
	});
	
	
	return WidgetEng;
	
	
	
});