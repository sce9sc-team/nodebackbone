define(['zepto','underscore','backboneAssociations'],
function($,_, Backbone)
{
	
	if(typeof WidgetEng == 'undefined')WidgetEng = {};
	
	Backbone.Model.prototype._super = function(method){
	  return this.constructor.__super__[method].apply(this, _.rest(arguments));
	};
	
	WidgetEng.Widget = Backbone.AssociatedModel.extend({
		 relations: [
		 {
	                type:Backbone.Many,
	                key:'widgets',
	                relatedModel:'WidgetEng.Widget'
	     }],

		defaults:{		
			tagName:'div',
			classname:"widget",	
			el:"widget",
			width:"100%",
			height:"100%",	
			border:"",
			background:'red',
			color:"white",
			widgets:[],
			parent:null,
			displayedPage:null,
			viewClass:'WidgetView',
			selectable:false,
			events:[],
			prevEvents:[]
		},
		initialize:function()
		{	
			this.addInitialView();
			this.historyChanges=[];
			this.currentHistoryIndex=0;
			this.undoFlag = false;
			this.on("add:widgets", function(widget){	
				if(widget.get('parent')==null)
				{
					widget.set({parent:this})
				}
				console.log(widget);
				widget.addView(widget);					
			});
			this.on('change',function(){
				if(!this.undoFlag){
					var newChanges = _.clone(this._currentAttributes)
					this.historyChanges.push(newChanges);
					this.currentHistoryIndex = this.historyChanges.length;
				}
			//this.on('change:events',function(){console.log(1)})

			})
		},
		addEventsToView:function(e){
			console.log(e)
		},
		addInitialView:function(){
			if(this.get('el')=='main'){	
				this.view = new WidgetEng[this.get("viewClass")]({el:$("#"+this.get('el')),model:this});			
			}	
		},
		addView:function()
		{
			console.log('aaa');
			this.view = new WidgetEng[this.get("viewClass")]({
					model:this,
					tagName: this.get('tagName'),
					className: this.get('classname'),
					id:this.get('el'),
					parent_el:$("#"+this.get('parent').get('el'))
				});
		},
		getWidgets:function(n)
		{
			return this.get(this.relations[0].key).models;
		},
		addWidget:function(widget,options)
		{
			 if(widget!=null)
			 {
			 	//if page not null aka you have passed params
			 	//add parent to params.
			 	//need to check if object model 
			 	if(widget.attributes==null)
			 	{
			 		widget.parent = this
			 	}
			 	var newWidget = this.get(this.relations[0].key).add(widget,options);
			 }else{
			 	widget={parent:this};
			 	var newWidget = this.get(this.relations[0].key).add(widget,options);
			 }
			return newWidget;//this returns the collection
		},
		getWidgetById:function(id)
		{
			var res = [];
			var currentSearchObj = this
			var findme =  function(obj,id)
			{	
				var fobj = obj.get(currentSearchObj.relations[0].key).where({el:id});	
				if(fobj.length==0)
				{
					if(obj.get(currentSearchObj.relations[0].key).length!=0)
					{
						for(var i=0;i<obj.get(currentSearchObj.relations[0].key).length;i++)
						{
							if(obj.getWidgets()[i].get(currentSearchObj.relations[0].key)!=null)
							{
								currentSearchObj = obj.getWidgets()[i]
								findme(currentSearchObj,id);
							}
						}
					}
				}
				else
				{
					res.push(fobj[0]);			
				}
			}		
			findme(currentSearchObj,id);
			return res;	
		},
		undo:function(){
			this.undoFlag = true;

			this.currentHistoryIndex=this.currentHistoryIndex-1;

			var changes = this.historyChanges[this.currentHistoryIndex-1];
			console.log(changes)
			this.set(changes);
			this.undoFlag = false;
		},
		removeEvents:function(ev){
			var currentEvents = this.get('events').slice();
			var prevEvents = currentEvents.slice();
			_.each(currentEvents, function(e,i,l){
				if(_.keys(e)[0]==_.keys(ev)[0])
				{
					if(_.values(e)[0]==_.values(ev)[0])
					{
						currentEvents.splice(i,1);
					}
				}
			});

			this.set({prevEvents:prevEvents});
			this.set({events:currentEvents});	
		},
		addEvents:function(ev){
			var newevents =[];
			var update = false;
			var currentEvents = this.get('events');
			_.each(currentEvents, function(e,i,l){
				if(!update){
					if(_.keys(e)[0]==_.keys(ev)[0])
					{
						e[_.keys(e)[0]] = ev[_.keys(ev)[0]];					
						newevents.push(e);
						update = true;
					}
					else{
						newevents.push(e);
					}
				}
			})
			if(!update)
			{
				newevents.push(ev);
			}
			this.set({prevEvents:currentEvents});
			this.set({events:newevents});		
		}

	});
	
	return WidgetEng;
	
	
});