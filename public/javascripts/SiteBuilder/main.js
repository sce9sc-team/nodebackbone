var WidgetEng = {};

require.config({
 	paths: {
    	zepto: 'lib/zepto',
    	underscore: 'lib/underscore',
    	backbone: 'lib/backbone',
		backboneAssociations: 'lib/backbone-associations',
		io:'/socket.io/socket.io',	
 	},
	shim:{
		zepto:{
			exports: '$'
		},
		underscore:{
			exports: '_'
		},
		backbone:{
			deps: ["underscore", "zepto"],
			exports: "Backbone"
		},
		backboneAssociations: {
	        deps: ['backbone'],
			exports: 'Backbone'
	    },
		io:{
			exports: 'io'
		}		
	}
});

require([
  // Load our app module and pass it to our definition function
  'app'	
], function(){
 	

	
});
