define(['models/box',
		'views/boxView',
		'models/hbox',
		'views/hboxView',
		'models/vbox',
		'views/vboxView',
		'models/site',
	    'views/siteView',
		'models/pageController',
		'views/pageControllerView',
		'models/page',	
		'views/pageView',
		'models/image',	
		'views/imageView',
		'models/customHtml',	
		'views/customHtmlView',
		'models/imageGallery',	
		'views/imageGalleryView',
		'events',
		], 
	function(WidgetEng)
  	{
  		//var s = require('/javascripts/SiteBuilder/models/box');
		WidgetEng.ParseDom = function(site)
		{
			
			function getChildren(w,current){
				var wgts = $('#'+ w.get('el')).children('[data-widget]');
				if(wgts.length>0)
				{
					for(var i=0;i<wgts.length;i++)
					{
						
						var oo = {};
						oo.el = wgts[i].id;
						oo.width = wgts[i].style.width;
						oo.height =wgts[i].style.height;
						oo.border =wgts[i].style.border;
						oo.display =(wgts[i].style.display=='none')?false:true;
						oo.background =wgts[i].style.background;
						(wgts[i].src)?oo.src = wgts[i].src:"";						
						(wgts[i].dataset['swipe'])?oo.swipe=wgts[i].dataset['swipe']:"";
						oo.parent = w.get('el');
						if(wgts[i].dataset.widget=="customHtml"){oo.html=wgts[i].innerHTML};					
						oo.flex_child =(wgts[i].className.split(" ").indexOf('flex_child_1')>-1)?true:false;
						oo.events=(wgts[i].dataset.events)?JSON.parse(wgts[i].dataset.events):[];
						var myel= new WidgetEng[wgts[i].dataset.widget](oo);
						current.addWidget(myel);
						getChildren(myel,myel);	
					}			
				}		
			};
			getChildren(site,site);
		}
		
		
		
		WidgetEng.Start = function(){
			WidgetEng.domParser = true;
			var siteEl = $('[data-widget="Site"]');
			var wigt = {el:siteEl[0].id,
						width:siteEl[0].style.width,
						height:siteEl[0].style.height,
						border:siteEl[0].style.border,
						background:siteEl[0].style.background,
						events :JSON.parse(siteEl[0].dataset.events),
						};
			WidgetEng.theSite = new WidgetEng.Site(wigt);
			//if(!localStorage['theSite']){
				WidgetEng.ParseDom(WidgetEng.theSite);
			//	localStorage.setItem("theSite", JSON.stringify(WidgetEng.theSite.toJSON()));
			//}else{
			//	console.log('----------THis is from local localStorage')
			//	var site = JSON.parse(localStorage['theSite']);
			//	WidgetEng.theSite.set(site);
			//}
			WidgetEng.domParser = false;
			document.getElementById("loadingScreen").style.display='none'
			//WidgetEng.widgetEngRouter.navigate('home/',{trigger: true});
		};
		WidgetEng.Start();
	
		
		return WidgetEng;
	
		
  	
	}
);