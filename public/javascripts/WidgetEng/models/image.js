define([	
	'jquery',
	'underscore',
	'backboneAssociations',
	'models/widget',
	'models/box',
	],
function($,_, Backbone,WidgetEng)
{
		
	WidgetEng.Image = WidgetEng.Box.extend({
		relations:[
	 {
                type:Backbone.Many,
                key:'widgets',
                relatedModel:'WidgetEng.Box'
     }], 
	defaults:_.extend({},WidgetEng.Widget.prototype.defaults,{
		tagName:'img',
		classname:"image",
		el:"img",
		viewClass: "imageView",
		background:"",
		src:"../images/test.png",
		objectClass:'Image',
		border:"1px solid black",
		handles:false,
		canResize:false,
		selectable:true
	}),

});
	
	return WidgetEng;
	
});
