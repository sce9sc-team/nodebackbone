define([
		'jquery',
		'underscore',
		'backboneAssociations',
		'models/widget'],
function($,_, Backbone,WidgetEng)
{
	WidgetEng.Site = WidgetEng.Widget.extend({
		defaults:_.extend({},WidgetEng.Widget.prototype.defaults,{
			classname:"site",
			el:"site",
			objectClass:'Site',
			position: "absolute",
			viewClass: "SiteView",
			pageController:null,
			currentPage:null
		}),		
		socket_events:{
			"updateSite":"updateSite"
		},
		postInitWidget:function(){
			this.initSockets();
		},
		updateSite:function(data){
			console.log('socket get pages' );
			console.log(data);
		},
		showPageById:function(id)
		{
			var wgt = this.getWidgetById(id)[0];
			if(wgt){
				//check if currentWidget is a page
				if(wgt.get('objectClass')=='Page')
				{
					var curPage = this.get('currentPage');
					if(curPage!=null)
					{
						function hidePages(w)
						{
							var p_wgt = WidgetEng.theSite.getWidgetById(w.get('parent'))[0];
							if(p_wgt.get('el')!=WidgetEng.theSite.get('pageController'))
							{
								p_wgt.set({display:false});
								hidePages(p_wgt);
							}
						}
						var cPage = this.getWidgetById(curPage)[0];
						hidePages(cPage);
						cPage.set({display:false});
					}
					function showPages(w)
					{
						var p_wgt = WidgetEng.theSite.getWidgetById(w.get('parent'))[0];
						if(p_wgt.get('el')!=WidgetEng.theSite.get('pageController'))
						{
							p_wgt.set({display:true});
							showPages(p_wgt);
						}
					}
					showPages(wgt);
					wgt.set({display:true});
					var childPageCont = wgt.get('widgets').where({objectClass:'PageController'})
					if(childPageCont.length>0)
					{
						childPageCont[0].set({display:true});
						if(childPageCont[0].get('currentPage'))
						{
							wgt.getWidgetById(childPageCont[0].get('currentPage'))[0].set({display:true});
						}else{
							var cur_sub_page = childPageCont[0].getWidgets()[0]
							cur_sub_page.set({display:true});
							childPageCont[0].set({'currentPage':cur_sub_page.get('el')});
						}
					}
					var wgtCont = WidgetEng.theSite.getWidgetById(wgt.get('parent'))[0];
					wgtCont.set({'currentPage':wgt.get('el')});
					this.set({'currentPage':wgt.get('el')});		
				}	
			}
		},
		saveSite:function()
		{
			try{
				var widgets = this.toJSON();
				WidgetEng.socket.emit('saveSite',widgets);
			}
			catch(err){
				console.log(err.message)
				
			}
		}		
	});
	
	
	return WidgetEng;
	
	
	
});