define([	
	'jquery',
	'underscore',
	'backboneAssociations',
	'models/widget',
	],
function($,_, Backbone,WidgetEng)
{
		
	WidgetEng.ImageGallery = WidgetEng.Widget.extend({
		relations:[
	 	{
                type:Backbone.Many,
                key:'widgets',
                relatedModel:'WidgetEng.Image'
     	}],
		defaults:_.extend({},WidgetEng.Widget.prototype.defaults,{
			classname:"imageGallery swiper-container",
			el:"imageGallery",
			viewClass: "ImageGalleryView",
			objectClass:'ImageGallery',
			background:"",
			border:"1px solid black",
			handles:false,
			canResize:false,
			selectable:true,
			mySwiper:null,
		}),
		addWidget:function(widget,options)
		{
			 if(widget!=null)
			 {
			 	//if page not null aka you have passed params
			 	//add parent to params.
			 	//need to check if this is an object created by a model or just params
			 	if(widget.attributes==null)
			 	{
			 		widget.parent = this.get('el')+'-wrapper';
					//widget.classname= this.get('classname')+' swiper-slide';	
			 	}else{
					widget.set({parent:this.get('el')+'-wrapper'})
					widget.set({classname:this.get('classname')+' swiper-slide'})
				}
			 }else{
			 	widget={parent:this.get('el')+'-wrapper'};	
				//widget ={classname:this.get('classname')+' swiper-slide'};		 	
			 }		
			var newWidget = this.get(this.relations[0].key).add(widget,options);
			return newWidget;//this returns the collection
		},
		postAddWidgets:function(widget){
			//function to be called after on add event is triggered
			console.log(this.model.get(mySwiper));
		},
		
	});

	
	return WidgetEng;
	
});