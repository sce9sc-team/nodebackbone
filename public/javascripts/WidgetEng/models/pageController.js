define([	
	'jquery',
	'underscore',
	'backboneAssociations',
	'models/widget',
	],
function($,_, Backbone,WidgetEng)
{
	WidgetEng.PageController = WidgetEng.Widget.extend({
		relations:[
	 	{
	                type:Backbone.Many,
	                key:'widgets',
	                relatedModel:'WidgetEng.Page'
	     }],
		defaults:_.extend({},WidgetEng.Widget.prototype.defaults,{
			classname:"pageController",	
			el:"pageController",
			viewClass: "PageControllerView",
			background:"yellow",		
			objectClass:'PageController',
			currentPage:null,		
		}),
		postInitWidget:function(){
			if(WidgetEng.theSite.get('pageController')==null){
				WidgetEng.theSite.set({pageController:this.get('el')});
			}
			this.initSockets();
		},
		postAddWidgets:function(widget){
			//function to be called after on add event is triggered
			if(widget.get('display'))
			{				
				var wgtId = widget.get('el');
				WidgetEng.theSite.set({currentPage:wgtId});
				this.set({currentPage:wgtId});
			}		
		},
		socket_events:{
			"pages":"setPages"
		},
		
		setPages:function(data)
		{
			console.log('socket get pages' );	
			this.get('widgets').update(data);	
		},
		addWidget:function(widget,options)
		{
			
			 if(widget!=null)
			 {
				//if you pass a widget Object , params only or a new Widget Object			 	
			 	if(widget.attributes==null)
			 	{
			 		//This is to check if it is a new Object, in this case it is not
					widget.parent = this.get('el');
					var newWidget = this.get(this.relations[0].key).add(widget,options);
					return newWidget;//this returns the collection
			 	}				
				
				if(this.relations[0].relatedModel=='WidgetEng.'+widget.get('objectClass'))
				{					
					var newWidget = this.get(this.relations[0].key).add(widget,options);					
					return newWidget;//this returns the collection
				}
				else
				{
					console.log('You can only add a Page to a Controller')
				}
			 }else{
				//if you dont pass params or a new Object
			 	widget={parent:this.get('el')};	
				var newWidget = this.get(this.relations[0].key).add(widget,options);
				return newWidget;//this returns the collection		 	
			 }		
			
		},				
	
	});
	
	return WidgetEng;
});