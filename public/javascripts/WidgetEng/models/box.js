define([	
	'jquery',
	'underscore',
	'backboneAssociations',
	'models/widget',
	],
function($,_, Backbone,WidgetEng)
{
		
	WidgetEng.Box = WidgetEng.Widget.extend({
		defaults:_.extend({},WidgetEng.Widget.prototype.defaults,{
			classname:"box",
			el:"box",
			viewClass: "BoxView",
			objectClass:'Box',
			background:"blue",
			border:"1px solid black",
			handles:false,
			canResize:false,
			selectable:true,
			box:null,
		}),
		
	});

	
	return WidgetEng;
	
});