define(['jquery','underscore','backboneAssociations','io'],
function($,_, Backbone)
{
	WidgetEng.socket = io.connect('http://localhost:3000');	
	if(typeof WidgetEng == 'undefined')WidgetEng = {};
	
	Backbone.Model.prototype._super = function(method){
	  return this.constructor.__super__[method].apply(this, _.rest(arguments));
	};
	WidgetEng.copiedObjects =[];
	WidgetEng.Widget = Backbone.AssociatedModel.extend({
		 relations: [
		 {
	                type:Backbone.Many,
	                key:'widgets',
	                relatedModel:'WidgetEng.Widget'
	     }],

		defaults:{		
			tagName:'div',
			classname:"widget",	
			el:"widget",
			width:"100%",
			height:"100%",	
			border:"",
			background:'red',
			flex_child:false,
			color:"white",
			position:"relative",
			wigt_pos:null,
			widgets:[],
			parent:null,
			display:true,
			objectClass:'Widget',
			viewClass:'WidgetView',
			swipe:false,
			selectable:false,
			events:[],
			prevEvents:[]
		},
		initialize:function()
		{	
			this.addInitialView();
			this.historyChanges=[];
			this.currentHistoryIndex=0;
			this.undoFlag = false;
			this.on("add:widgets", function(widget){
				console.log('widget Add event');
				// add the widget Position so that the view will know where to render it
				if(widget.get('wigt_pos')==null){
					widget.set({'wigt_pos':_.indexOf(this.getWidgets(),widget)});
				}
				if(!widget.view){ //if the view of the widget does not exist then it is a widget created using javascript
					
					if(widget.get('parent')==null)
					{
						widget.set({parent:this.get('el')})
					}
					//widget.set({wigt_pos:null,
					widget.addView();	
					
					function addChildView(p,wgts){
						if(wgts.length>0)
						{
							for(var i=0;i<wgts.length;i++)
							{
								wgts[i].set({'parent':p.get('el')});
								wgts[i].addView();	
								addChildView(wgts[i],wgts[i].getWidgets());
							}
						}
					}
					if(widget['getWidgets']!=null){
						//custom html widget doesnot have widgets
						addChildView(widget,widget.getWidgets());
					}
					
				}
					this.postAddWidgets(widget);	
			});
			this.on('change',function(){
				if(!this.undoFlag){
					var newChanges = _.clone(this._currentAttributes)
					this.historyChanges.push(newChanges);
					this.currentHistoryIndex = this.historyChanges.length;
				}
			});
			//this.get('widgets').on('change',function(){alert('widget')})
			this.postInitWidget();
		},
		socket_events:{},
		postInitWidget:function(){},
		postAddWidgets:function(widget){//function to be called after on add event is triggered
			},
		addEventsToView:function(e){
			console.log(e)
		},
		addInitialView:function(){
			if($("#"+this.get('el')).length!=0)	{ //check if the dom element exists if yes then create the view 
				this.view = new WidgetEng[this.get("viewClass")]({el:$("#"+this.get('el')),model:this});			
			}/*else{
				this.addView();
			}*/
		},
		initSockets: function()
		{
			if (this.socket_events && _.size(this.socket_events) > 0) {
				this.delegateSocketEvents(this.socket_events);
			}	
		},
		delegateSocketEvents: function (events) {
			for (var key in events) 
			{
				var method = events[key];
				if (!_.isFunction(method)) {
					method = this[events[key]];
				}
				if (!method) {
					throw new Error('Method "' + events[key] + '" does not exist');
				}
				method = _.bind(method, this);
				WidgetEng.socket.on(key, method);
			}
		},
		addView:function()
		{
			console.log('Adding view to the Widget')
			this.view = new WidgetEng[this.get("viewClass")]({
					model:this,
					tagName: this.get('tagName'),
					className: this.get('classname'),
					id:this.get('el'),
					parent_el:$("#"+this.get('parent'))
				});
		},
		getWidgets:function(n)
		{
			return this.get(this.relations[0].key).models;
		},
		getParent:function(){
			return WidgetEng.theSite.getWidgetById(this.get('parent'))[0];
		},
		addWidget:function(widget,options)
		{
			 if(widget!=null)
			 {
			 	//if page not null aka you have passed params
			 	//add parent to params.
			 	//need to check if this is an object created by a model or just params
			 	if(widget.attributes==null)
			 	{
			 		widget.parent = this.get('el');
			 	}			 
			 }else{
			 	widget={parent:this.get('el')};			 	
			 }		
			var newWidget = this.get(this.relations[0].key).add(widget,options);
			return newWidget;//this returns the collection
		},
		getWidgetById:function(id)
		{
			var res = [];
			var currentSearchObj = this
			var findme =  function(obj,id)
			{	
				var fobj = obj.get(currentSearchObj.relations[0].key).where({el:id});	
				if(fobj.length==0)
				{
					if(obj.get(currentSearchObj.relations[0].key).length!=0)
					{
						for(var i=0;i<obj.get(currentSearchObj.relations[0].key).length;i++)
						{
							if(obj.getWidgets()[i].get(currentSearchObj.relations[0].key)!=null)
							{
								currentSearchObj = obj.getWidgets()[i]
								findme(currentSearchObj,id);
							}
						}
					}
				}
				else
				{
					res.push(fobj[0]);			
				}
			}		
			findme(currentSearchObj,id);
			return res;	
		},
		undo:function(){
			this.undoFlag = true;

			this.currentHistoryIndex=this.currentHistoryIndex-1;

			var changes = this.historyChanges[this.currentHistoryIndex-1];
			
			this.set(changes);
			this.undoFlag = false;
		},
		removeEvents:function(ev){
			var currentEvents = this.get('events').slice();
			var prevEvents = currentEvents.slice();
			_.each(currentEvents, function(e,i,l){
				if(_.keys(e)[0]==_.keys(ev)[0])
				{
					if(_.values(e)[0]==_.values(ev)[0])
					{
						currentEvents.splice(i,1);
					}
				}
			});

			this.set({prevEvents:prevEvents});
			this.set({events:currentEvents});	
		},
		addEvents:function(ev){
			var newevents =[];
			var update = false;
			var currentEvents = this.get('events');
			_.each(currentEvents, function(e,i,l){
				if(!update){
					if(_.keys(e)[0]==_.keys(ev)[0])
					{
						e[_.keys(e)[0]] = ev[_.keys(ev)[0]];					
						newevents.push(e);
						update = true;
					}
					else{
						newevents.push(e);
					}
				}
			})
			if(!update)
			{
				newevents.push(ev);
			}
			this.set({prevEvents:currentEvents});
			this.set({events:newevents});	

		},
		copy:function(page)
		{
			var copyObj = this.clone();
			var elname = copyObj.get('el');
			copyObj.set({el:elname+'_copy'});
			WidgetEng.copiedObjects.push(copyObj);
			return copyObj;

		},
		remove:function()
		{
			var parent = WidgetEng.theSite.getWidgetById(this.get('parent'));
			parent.get('widgets').remove(this);
		}
		

	});
	
	return WidgetEng;
	
	
});