define([	
	'jquery',
	'underscore',
	'backboneAssociations',
	'models/widget',
	'models/box',
	],
function($,_, Backbone,WidgetEng)
{

	WidgetEng.vBox = WidgetEng.Widget.extend({
		relations:[
		 {
	                type:Backbone.Many,
	                key:'widgets',
	                relatedModel:'WidgetEng.Box'
	     }],
		defaults:_.extend({},WidgetEng.Widget.prototype.defaults,{
			classname:"vbox",
			el:"vbox",
			viewClass: "vBoxView",
			background:"blue",
			objectClass:'vBox',
			border:"1px solid black",
			handles:false,
			canResize:false,
			selectable:true
		}),
	});
	
	return WidgetEng;

});