define([	
	'jquery',
	'underscore',
	'backboneAssociations',
	'models/widget',	
	],
function($,_, Backbone,WidgetEng)
{

	WidgetEng.customHtml = Backbone.AssociatedModel.extend({
	
		defaults:{		
			tagName:'div',
			classname:"customHtml",	
			el:"customHtml",
			width:"",
			height:"",	
			border:"",
			background:'',
			flex_child:false,
			color:"",
			position:"relative",		
			parent:null,
			display:true,
			objectClass:'customHtml',
			viewClass:'customHtmlView',
			selectable:false,
			events:[],
			wigt_pos:null,
			prevEvents:[],
			html:'',
			
		},
		initialize:function()
		{	
			console.log('init customHTML widget')
			this.addInitialView();
			this.historyChanges=[];
			this.currentHistoryIndex=0;
			this.undoFlag = false;			
			this.on('change',function(){
				if(!this.undoFlag){
					var newChanges = _.clone(this._currentAttributes)
					this.historyChanges.push(newChanges);
					this.currentHistoryIndex = this.historyChanges.length;
				}
			})
			this.postInitWidget();
		},
		postInitWidget:function(){},
		postAddWidgets:function(widget){
			//function to be called after on add event is triggered
		},		
		addInitialView:function(){
			console.log('addInitialView customHTML widget')
			if($("#"+this.get('el')).length!=0)	{ //check if the dom element exists if yes then create the view 
				this.view = new WidgetEng[this.get("viewClass")]({el:$("#"+this.get('el')),model:this});			
			};
		},
		addView:function()
		{
			console.log('Adding view to the Widget')
			this.view = new WidgetEng[this.get("viewClass")]({
					model:this,
					tagName: this.get('tagName'),
					className: this.get('classname'),
					id:this.get('el'),
					parent_el:$("#"+this.get('parent'))
				});
		},		
		undo:function(){
			this.undoFlag = true;

			this.currentHistoryIndex=this.currentHistoryIndex-1;

			var changes = this.historyChanges[this.currentHistoryIndex-1];
			
			this.set(changes);
			this.undoFlag = false;
		},
		removeEvents:function(ev){
			var currentEvents = this.get('events').slice();
			var prevEvents = currentEvents.slice();
			_.each(currentEvents, function(e,i,l){
				if(_.keys(e)[0]==_.keys(ev)[0])
				{
					if(_.values(e)[0]==_.values(ev)[0])
					{
						currentEvents.splice(i,1);
					}
				}
			});

			this.set({prevEvents:prevEvents});
			this.set({events:currentEvents});	
		},
		addEvents:function(ev){
			var newevents =[];
			var update = false;
			var currentEvents = this.get('events');
			_.each(currentEvents, function(e,i,l){
				if(!update){
					if(_.keys(e)[0]==_.keys(ev)[0])
					{
						e[_.keys(e)[0]] = ev[_.keys(ev)[0]];					
						newevents.push(e);
						update = true;
					}
					else{
						newevents.push(e);
					}
				}
			})
			if(!update)
			{
				newevents.push(ev);
			}
			this.set({prevEvents:currentEvents});
			this.set({events:newevents});	

		},
		copy:function(page)
		{
			var copyObj = this.clone();
			var elname = copyObj.get('el');
			copyObj.set({el:elname+'_copy'});
			WidgetEng.copiedObjects.push(copyObj);
			return copyObj;

		},
		remove:function()
		{
			var parent = WidgetEng.theSite.getWidgetById(this.get('parent'))[0];
			console.log(parent)
			parent.get('widgets').remove(this);
		}
		
	});

		return WidgetEng;
});