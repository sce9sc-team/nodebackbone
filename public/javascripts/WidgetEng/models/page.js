define([
		'jquery',
		'underscore',
		'backboneAssociations',
		'models/widget',
		'models/pageController'],
function($,_, Backbone,WidgetEng)
{
	WidgetEng.Page = WidgetEng.Widget.extend({
		relations: [
			{
		                type:Backbone.Many,
		                key:'widgets',
		                relatedModel:'WidgetEng.Widget'
		     },
	        {
	            type: Backbone.One, //nature of the relationship
	            key: 'pageController', 
	            relatedModel: 'WidgetEng.PageController' //AssociatedModel for attribute key
	        }
	    ],	
		defaults:_.extend({},WidgetEng.Widget.prototype.defaults,{
			classname:"page",
			el:"page",
			pageController:null,
			viewClass: "PageView",
			objectClass:'Page',	
			display:false,			
		}), 
		socket_events:{
			"updatePage":"updatePage"
		},
		savePage:function(){
			try{
					var widgets = this.toJSON();
					widgets.display = false;
					console.log(widgets);
					WidgetEng.socket.emit('savePage',widgets);
			}
			catch(err){
				console.log(err.message)

			}
		},
		postInitWidget:function(){
			this.initSockets();
		},
		updatePage:function(data){
			console.log('socket get pages' );
			console.log(data);
			if(data.el == this.get('el'))
			{
				
				console.log('--------------------------')
				var widgets = data.widgets;
				data.display = this.get('display');
				this.get('widgets').update(data.widgets)
				/*var parent = this.getParent();
				console.log(parent)
				parent.get('widgets').set(this)
				parent.addWidget(data);*/
				this.set(data);
			}
			//this.set(data)//.reset({'widgets':data});
		},
		addWidget:function(widget,options)
		{
			 if(widget!=null)
			 {
			  	if(widget.attributes==null)
			 	{
			 		widget.parent = this.get('el');
			 	}
				
				//If it is a page Controller then:
				if(this.relations[1].relatedModel=='WidgetEng.'+widget.get('objectClass'))
				{
					//You can only have one page Controller on a page
					if(this.get('pageController')==null){
						this.set({'pageController':newWidget});	
						var newWidget = this.get(this.relations[0].key).add(widget,options);
						return newWidget;//this returns the collection
					}else{console.log("There is already a page Controller on this page.")}		
				}else{
					//If it is a Widget then:
					var newWidget = this.get(this.relations[0].key).add(widget,options);
					return newWidget;//this returns the collection
				}
			
			
			 }else{
				//If it is not an Object what is passed then it is a Widget:
			 	widget={parent:this.get('el')};	
				var newWidget = this.get(this.relations[0].key).add(widget,options);
				return newWidget;//this returns the collection		 	
			 }		
			
		},
	});

	return WidgetEng;
	
	
	
});