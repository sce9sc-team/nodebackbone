define([	
	'jquery',
	'underscore',
	'backboneAssociations',
	'models/widget',
	'models/box',
	],
function($,_, Backbone,WidgetEng)
{
		
	WidgetEng.hBox = WidgetEng.Box.extend({
		relations:[
	 {
                type:Backbone.Many,
                key:'widgets',
                relatedModel:'WidgetEng.Box'
     }],
	defaults:_.extend({},WidgetEng.Widget.prototype.defaults,{
		classname:"hbox",
		el:"hbox",
		viewClass: "hBoxView",
		background:"blue",
		objectClass:'hBox',
		border:"1px solid black",
		handles:false,
		canResize:false,
		selectable:true
	}),

});
	
	return WidgetEng;
	
});

