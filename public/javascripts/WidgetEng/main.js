var WidgetEng = {};

require.config({
 	paths: {
    		jquery:'lib/jquery_1_9_1_min',
    	underscore: 'lib/underscore_1_4_4_min',
    	backbone: 'lib/Backbone_0_9_10_min',
		backboneAssociations: 'lib/backbone-associations',
		io:'/socket.io/socket.io',	
		iscroll: 'lib/iscroll',
		swiper:'lib/idangerous.swiper-1.8.min'
 	},
	shim:{	
		jquery:{
			exports: '$'
		},
		underscore:{
			exports: '_'
		},
		backbone:{
			deps: ["underscore", "jquery"],
			exports: "Backbone"
		},
		backboneAssociations: {
	        deps: ['backbone'],
			exports: 'Backbone'
	    },
		io:{
			exports: 'io'
		}		
	}
});

require(['routes/widgetEngRouter',
  // Load our app module and pass it to our definition function
  'app'	
], function(WidgetEng,io){
	
	
	WidgetEng.widgetEngRouter = new WidgetEng.WidgetEngRouter();
	//console.log(window.location.hash.replace('#',''))
	window.addEventListener('scroll', function(){
		//Event listener to detect Scroll
		WidgetEng.scrollmove = true;
		
	}, false);
	//alert(window.devicePixelRatio)
	if(window.location.hash!=''){
		WidgetEng.theSite.showPageById(window.location.hash.replace('#','').replace("/",''))
		//WidgetEng.widgetEngRouter.navigate(window.location.hash.replace('#',''),{trigger: true});
	}
	
	
});

