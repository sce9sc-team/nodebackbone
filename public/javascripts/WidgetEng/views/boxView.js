define([
		'jquery',
		'underscore',
		'backboneAssociations',
		'models/widget',
		'views/widgetView',
		],
function($,_, Backbone,WidgetEng)
{

	WidgetEng.BoxView = WidgetEng.WidgetView.extend({

		postRender:function(){
			this.el.style.overflow = 'hidden';
			this.el.style.boxSizing="border-box";
			this.el.style.MozBoxSizing="border-box";
			this.el.style.webkitBoxSizing="border-box";	
				
				
			return this; //recommended as this enables calls to be chained.
		},
		
	});
	
	return WidgetEng;
	
});