define([
		'jquery',
		'underscore',
		'backboneAssociations',
		'models/widget',
		'views/widgetView',	
		],
function($,_, Backbone,WidgetEng)
{

	WidgetEng.customHtmlView = WidgetEng.WidgetView.extend({
			initialize: function(){
			  		console.log('initializing Widget');	
					if(this.model.get('parent'))
					{						
						var wigt_pos = this.model.get('wigt_pos');
						if(wigt_pos!=null)
						{
							var parent = $('#'+this.model.get('parent'));
							if(wigt_pos < parent.children().length )
							{	
								console.log('----'+wigt_pos +'----'+parent.children().length)
								$(parent.children()[wigt_pos]).before(this.el);//append this widget before 
							}else{
								$('#'+this.model.get('parent')).append(this.el); //append this widget to the end 
							}
						}else{					
							$('#'+this.model.get('parent')).append(this.el); //append this widget to the end
						}
					}   
		            this.listenTo(this.model, "remove", this.remove); //remove the view when object is removed        

		            this.listenTo(this.model, "change:el", this.updateEl); //update el when the objects el has changed
					this.listenTo(this.model, "change:background", function(){this.el.style.background = this.model.get('background') });
					this.listenTo(this.model, "change:width", function(){this.el.style.width = this.model.get('width') });
					this.listenTo(this.model, "change:height", function(){this.el.style.height = this.model.get('height') });
					this.listenTo(this.model, "change:border", function(){this.el.style.border = this.model.get('border') });
					this.listenTo(this.model, "change:color", function(){this.el.style.color = this.model.get('color') });
					this.listenTo(this.model, "change:html", function(){this.el.innerHTML = this.model.get('html') });
					this.listenTo(this.model, "change:display", function(){this.el.style.display = (this.model.get('display'))?'block':'none';});
					this.listenTo(this.model, "change:flex_child", function(){ 
						var cl = this.el.className.split(' ');
						if(this.model.get('flex_child'))
						{	this.el.className = _.union(cl, ['flex_child_1']).join(' ');}
						else{
							this.el.className = _.without(cl,'flex_child_1').join(' ');;
						}
					 });
					this.listenTo(this.model, "change:events", this.eventsChange);				
					this.render();
		    },
			render: function()
			{
				
				this.el.className = _.union(this.el.className.split(" "),(this.el.flex_child)?['flex_child_1']:[],[this.model.get('classname')]).join(" ")
				this.el.style.width = this.model.get('width');
				this.el.style.height = this.model.get('height');
				this.el.style.border = this.model.get('border');
				this.el.style.background = this.model.get('background');
				this.el.style.position = this.model.get('position');
				this.el.style.display = (this.model.get('display'))?'block':'none';
				this.el.innerHTML = this.model.get('html');
				this.el.dataset.events = JSON.stringify(this.model.get('events'));
				this.el.dataset.selectable = "true";
				this.el.dataset.widget = this.model.get('objectClass');
				this.addInitEvents();
				this.postRender();
				return this; //recommended as this enables calls to be chained.
			}
			
			
	});
	
	return WidgetEng;
	
});