define([
		'jquery',
		'underscore',
		'backboneAssociations',
		'models/widget',
		'views/widgetView',
		],
function($,_, Backbone,WidgetEng)
{
	
	WidgetEng.PageControllerView = WidgetEng.WidgetView.extend({

			events: {
	    	//"touchstart .pageController":"swipeStartEvent",
	    	//"touchmove .pageController":"swipeMoveEvent"
	    	},
			parseRen:function(wgtV){
				//console.log(wgtV.model.get('swipe'));
				if(wgtV.model.get('swipe')=='true'){
					this.addSwipe(wgtV);
				}
			},
			postRender:function(wgtV){	
				this.el.dataset.swipe = wgtV.model.get('swipe');
				if(wgtV.model.get('swipe')=='true'){			
					this.addSwipe(wgtV);
				}
			},
			swipe:function(controller,direction,wgt)
			{
				var pageCol = wgt.get('widgets');
				var page_length = pageCol.length;
				var prevPageId = wgt.get('currentPage');
				var prevPage = wgt.get('widgets').where({el:prevPageId})[0];
				var prevPagePos= prevPage.get('wigt_pos');	
				if(direction=='r')
				{
					if(prevPagePos+1<page_length)
					{
						var NextPage = pageCol.where({wigt_pos:prevPagePos+1})[0];
						WidgetEng.widgetEngRouter.navigate(NextPage.get('el')+'/',{trigger: true});
					}
					
				}
				else{
					if(prevPagePos!=0)
					{
						var NextPage = pageCol.where({wigt_pos:prevPagePos-1})[0];
						WidgetEng.widgetEngRouter.navigate(NextPage.get('el')+'/',{trigger: true});
					}				
				}
								
			},			
			swipeStartEvent:function(e){
				console.log(e)
			},
			swipeMoveEvent:function(e)
			{
				console.log(e)
			},
			addSwipe: function(wgtV,dir)
				{
				
				
					var k = wgtV.el;
					WidgetEng.movebias = (window.devicePixelRatio * 100);
					
					if(window.navigator.userAgent.toLowerCase().search("android")>-1)
					{	
						WidgetEng.movebias = 25;
						if (window.navigator.userAgent.toLowerCase().search("gt-p1000")>-1)
						{
							WidgetEng.movebias = 70;
						}
						if (window.navigator.userAgent.toLowerCase().search("a100")>-1)
						{
							WidgetEng.movebias = 7;
						}
						if (window.navigator.userAgent.toLowerCase().search("gt-i9300")>-1)
						{
							WidgetEng.movebias = 7;
						}
					}
					if (window.navigator.userAgent.toLowerCase().search("blackberry")>-1)
					{
						WidgetEng.movebias = 7;
					}
					
					function doSwipe(fn,e,dir,w)
					{

						if (k.anim) 
						{
							k.anim = false; 
							fn(e.target,dir,w); 
						}
					}

					k.anim    = false;
					k.capture = false;

					if('ontouchstart' in document)
					{
						k.ontouchstart= function(e)
						{				
							WidgetEng.scrollmove = false;

							k.swipeStart =	k.swipeMove  =	((dir === undefined)|| (dir === "lr") ? e.targetTouches[0].pageX : e.targetTouches[0].pageY);
							k.sOppositeStart = k.sOppositeMove = 	((dir === undefined)|| (dir === "lr") ? e.targetTouches[0].pageY : e.targetTouches[0].pageX);
							k.pageYOffset1 = k.pageYOffset2 = window.pageYOffset;

							k.anim = true;
							k.noAnim= true;
							if (!e) var event = window.event;
							e.cancelBubble = true;
							if(dir!==undefined)e.preventDefault();
							if (e.stopPropagation) e.stopPropagation();			
						};

						k.ontouchmove = function(e)
						{

							if (!e) var event = window.event;
							e.cancelBubble = true;
							if (e.stopPropagation) e.stopPropagation();	

							if (Math.abs(k.swipeMove-k.swipeStart) > 30)
							{
								e.preventDefault();
							}

							if (k.swipeMove-k.swipeStart < WidgetEng.movebias) 
							{
								k.noAnim= true;
							}else{

								if (k.swipeMove-k.swipeStart >-WidgetEng.movebias) 
								{
									k.noAnim= true;			
								}			
							}

							k.swipeMove = (dir === undefined ? e.targetTouches[0].pageX : e.targetTouches[0].pageY);
							k.sOppositeMove = ((dir === undefined)|| (dir === "lr") ? e.targetTouches[0].pageY : e.targetTouches[0].pageX);

							k.pageYOffset2 = window.pageYOffset;


							if (k.swipeMove-k.swipeStart > WidgetEng.movebias) 
							{ 		
								k.noAnim= false;
								if(!WidgetEng.scrollmove){
									if(Math.abs(k.sOppositeMove -k.sOppositeStart)<30){		
										if(Math.abs(k.pageYOffset2 -k.pageYOffset1)<30){						
											doSwipe(wgtV.swipe,e,'l',wgtV.model); 	
										}
									}

								}
							}
							else
							{				
								if (k.swipeMove-k.swipeStart <-WidgetEng.movebias) 
								{				
									k.noAnim= false;
									if(!WidgetEng.scrollmove){
										if(Math.abs(k.sOppositeMove -k.sOppositeStart)<30){		
											if(Math.abs(k.pageYOffset2 -k.pageYOffset1)<30){	
												doSwipe(wgtV.swipe,e,'r',wgtV.model); 							
											}
										}
									}
								}
							}


						};
					}
					else
					{	
						k.onmousedown = function(e)
						{
							e.preventDefault();
							k.swipeStart =
							k.swipeMove  =
							e.pageX;
							k.anim =
							k.capture =
							true;
							e.cancelBubble = true;
							if (e.stopPropagation) e.stopPropagation();
						};

						k.onmousemove = function(e)
						{
							if (k.capture)
							{
								if (Math.abs(k.swipeMove-k.swipeStart) > 5)
								{
									e.preventDefault();
								}
								k.swipeMove = e.pageX;
								if (k.swipeMove-k.swipeStart > 300){
									k.swipeStart =k.swipeMove;
									doSwipe(wgtV.swipe,e,'r',wgtV.model);
								}
								else{
									
									if (k.swipeMove-k.swipeStart <-300){
										console.log(k.swipeMove-k.swipeStart);
										k.swipeStart =k.swipeMove;
										doSwipe(wgtV.swipe,e,'l',wgtV.model);
									}
								}
								e.cancelBubble = true;
								if (e.stopPropagation) e.stopPropagation();		
							}
						};

						k.onmouseup   = function(e)
						{
							k.capture = false;
						};
					}
				}
			
			
			
		});
	
});