define([	
	'jquery',
	'underscore',
	'backboneAssociations',
	'models/widget',
	'views/widgetView',
	],
function($,_, Backbone,WidgetEng)
{

	WidgetEng.imageView = WidgetEng.WidgetView.extend({		
			postRender:function(){
				//this should in the begining
				this.el.src= this.model.get('src');
			},
		});
	
});