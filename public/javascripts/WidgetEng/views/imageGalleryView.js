define([
		'jquery',
		'underscore',
		'backboneAssociations',
		'models/widget',
		'views/widgetView',
		'swiper'
		],
function($,_, Backbone,WidgetEng,Swipper)
{

	WidgetEng.ImageGalleryView = WidgetEng.WidgetView.extend({
		//events: {
		//    "click .imageCont": "imageClick",
		//},
		template: _.template([
		    "<ul class='task_list'>",
		      "<% _.each(items,function(item,key,list) { %>",
		        "<%= itemTemplate(item) %>",
		      "<% }); %>",
		    "</ul>"
		  ].join('')),
		itemTemplate: _.template(
		    '<li class="<%= classname %>" id="<%= id %>"><%= name %></li>'
		  ),
		postRender:function(){
			this.el.style.overflow = 'hidden';
			this.el.style.boxSizing="border-box";
			this.el.style.MozBoxSizing="border-box";
			this.el.style.webkitBoxSizing="border-box";
			var wrapper = document.createElement('div');
			wrapper.id=this.el.id+'-wrapper';
			wrapper.className = 'swiper-wrapper'
			this.el.appendChild(wrapper);
			var mySwiper = new Swiper(this.el.className,{ 
				speed:750, 
				mode:'vertical'
			});  
			this.model.set({'mySwiper':mySwiper});
			/*var html = this.template({
			      items: [{id:'img1',classname:'imageCont',name:"stavros"},{id:'img2',classname:'imageCont',name:"alexis"}] ,
			      itemTemplate: this.itemTemplate
			    });
			this.$el.html(html);
			*/
			
			return this; //recommended as this enables calls to be chained.
		},
		imageClick:function(ss,sss)
		{
			console.log(ss);
		}
		
		
	});
	
	return WidgetEng;
	
});