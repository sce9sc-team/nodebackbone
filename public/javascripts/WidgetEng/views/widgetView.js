define([
		'jquery',
		'underscore',
		'backboneAssociations',
		'models/widget'
		],
function($,_, Backbone,WidgetEng)
{
	WidgetEng.WidgetView = Backbone.View.extend({	

			initialize: function(){
			  		console.log('initializing Widget');	
					if(this.model.get('parent'))
					{						
						var wigt_pos = this.model.get('wigt_pos');
						if(wigt_pos!=null)
						{
							var parent = $('#'+this.model.get('parent'));
							if(wigt_pos < parent.children().length )
							{	
								console.log('----'+wigt_pos +'----'+parent.children().length)
								$(parent.children()[wigt_pos]).before(this.el);//append this widget before 
							}else{
								$('#'+this.model.get('parent')).append(this.el); //append this widget to the end 
							}
						}else{					
							$('#'+this.model.get('parent')).append(this.el); //append this widget to the end
						}
					}	          
		            this.listenTo(this.model, "remove", function(){this.remove; console.log('view was removed'+this.el.id)}); //remove the view when object is removed        

		            this.listenTo(this.model, "change:el", this.updateEl); //update el when the objects el has changed
					this.listenTo(this.model, "change:background", function(){this.el.style.background = this.model.get('background') });
					this.listenTo(this.model, "change:width", function(){this.el.style.width = this.model.get('width') });
					this.listenTo(this.model, "change:height", function(){this.el.style.height = this.model.get('height') });
					this.listenTo(this.model, "change:border", function(){this.el.style.border = this.model.get('border') });
					this.listenTo(this.model, "change:color", function(){this.el.style.color = this.model.get('color') });
					this.listenTo(this.model, "change:display", function(){(this.model.get('display'))?this.el.style.display='':this.el.style.display='none';});
					this.listenTo(this.model, "change:flex_child", function(){ 
						var cl = this.el.className.split(' ');
						if(this.model.get('flex_child'))
						{	this.el.className = _.union(cl, ['flex_child_1']).join(' ');}
						else{
							this.el.className = _.without(cl,'flex_child_1').join(' ');;
						}
					 });
					this.listenTo(this.model, "change:events", this.eventsChange);				
					if(!WidgetEng.domParser){this.render();}else{						
						this.parseRen(this);
					}
					this.addInitEvents();
					
		    },
			parseRen:function(wgt){
				
			},
			render: function()
			{
				console.log('start Rendering WIdget');	
				this.el.className = this.model.get('classname');//_.union(this.el.className.split(" "),(this.el.flex_child)?['flex_child_1']:[],[this.model.get('classname')]).join(" ")
				this.el.style.width = this.model.get('width');
				this.el.style.height = this.model.get('height');
				this.el.style.border = this.model.get('border');
				this.el.style.background = this.model.get('background');
				this.el.style.position = this.model.get('position');
				(this.model.get('display'))?this.el.style.display='':this.el.style.display='none';
				this.el.dataset.events = JSON.stringify(this.model.get('events'));
				this.el.dataset.selectable = "true";
				this.el.dataset.widget = this.model.get('objectClass');
				
				this.postRender(this);
				return this; //recommended as this enables calls to be chained.
			}, 
			postRender:function(){
				//this is in order to pass the same params and add more if needed to each widget
			},
			updateEl:function(widget)
			{
				console.log('PageController updateEl event triggered');
				this.$el.attr('id',widget.get('el'))		
			},
			addInitEvents:function(){
				var events = this.model.get('events');
				var eventsInit = {}
				for(var i=0;i<events.length;i++)
				{
					var eventName = _.values(events[i])[0];
					var eventkey = _.keys(events[i])[0];
					this[eventName] = WidgetEng.customEvents[eventName];
					eventsInit[eventkey] = eventName;
				}
				this.delegateEvents(eventsInit);
				console.log();
			},
			eventsChange:function(wgt)
			{
				var prevEvents = wgt.get('prevEvents');
				for(var i=0;i<prevEvents.length;i++)
				{
					delete this[prevEvents[i][_.keys(prevEvents[i])[0]]];
				}
				this.undelegateEvents();

				var events = {};
				for(var i=0;i<wgt.changed.events.length;i++)
				{
					var eventName = _.values(wgt.changed.events[i])[0];

					var eventkey = _.keys(wgt.changed.events[i])[0];
					this[eventName] = WidgetEng.customEvents[eventName];
					events[eventkey] = eventName;
				}
				console.log(events);
				this.delegateEvents(events);
				this.el.dataset.events=JSON.stringify(wgt.get('events'));
			},
			fireCustomEvent:function(eventName)
			{
				var eventsArray = Object.keys(WidgetEng.customEvents);
				if(eventsArray.indexOf(eventName)>-1)
				{
					WidgetEng.customEvents[eventName]();
				}
			}
		

		});
	
	return WidgetEng;
	
});