define(['jquery','underscore','backboneAssociations','models/widget'],
function($,_, Backbone,WidgetEng)
{
		Backbone.history.start();
		
		WidgetEng.WidgetEngRouter = Backbone.Router.extend({
			routes: {
			"home/" : "showHomePage",
			"about1/" : "showAbout1",
			"*other/" : "defaultRoute"		
			},		
			showAbout1: function(){WidgetEng.mainController.showPage(1)},				
			showHomePage:function(){
				console.log(WidgetEng.theSite);
			},
			defaultRoute: function(other){		
				console.log(other)		
				WidgetEng.theSite.showPageById(other)
			}
		});
		
		return WidgetEng;
			
});