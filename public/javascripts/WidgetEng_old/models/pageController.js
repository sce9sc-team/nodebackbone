define(['zepto','underscore','backbone','backboneAssociations'],
function($,_, Backbone)
{
	
		WidgetEng.PageController = Backbone.AssociatedModel.extend({
			 relations: [
			 	{
		                type:Backbone.Many,
		                key:'pages',
		                relatedModel:'WidgetEng.Page'
		     }],

			url:'/api/test',
			defaults:{		
				tagName:'div',
				classname:"pageController",	
				el:"testController",
				width:"100%",
				height:"100%",	
				pages:[],
				parent:null,
				displayedPage:null
	
			},
			socket_events:{
				"pages":"setPages"
			},
			initialize: function(){
				this.get('pages').url= this.url;
				console.log('Initializing a Page controller'); 		
				//Creating a View for the Controller, if main Controller then : the div already exists
				if(this.get('el')=='main'){	
					this.view = new WidgetEng.PageControllerView({el:$("#"+this.get('el')),model:this});			
				}
			 	this.on("error", function(model, error){
					console.log(error); 
				}); 
				this.on("add:pages", function(page){	
					if(page.get('parent')==null)
					{page.set({parent:this})}
					console.log(page);
					page.addView();					
				});
				this.on("change:displayedPage",function(){
					//this.get('displayedPage').set({'displayed':true});		
				})
				this.initSockets()
		
			},
			initSockets: function()
			{
				if (this.socket_events && _.size(this.socket_events) > 0) {
					this.delegateSocketEvents(this.socket_events);
				}	
			},
			delegateSocketEvents: function (events) {
				for (var key in events) 
				{
					var method = events[key];
					if (!_.isFunction(method)) {
						method = this[events[key]];
					}
					if (!method) {
						throw new Error('Method "' + events[key] + '" does not exist');
					}
					method = _.bind(method, this);
					WidgetEng.socket.on(key, method);
			}
			},
			setPages:function(data)
			{
				console.log('socket get pages' );
				console.log(data);
				console.log(this);	
				this.get('pages').update(data.pages)
				this.showPage(0); // this is on order to set the view of the page // maybe it will be better to make an event 
				//or the above
				//test.myGalleryRouter.navigate('home/',{trigger: true});	
			},
	
			addView:function()
			{
				this.view = new WidgetEng.PageControllerView({
						model:this,
						tagName: this.get('tagName'),
						className: this.get('classname'),
						id:this.get('el'),
						parent_el:$("#"+this.get('parent').get('el'))
					});
			},
			addPage:function(page,options)
			{
				 if(page!=null)
				 {
				 	//if page not null aka you have passed params
				 	//add parent to params.
				 	//need to check if object model 
				 	if(page.attributes==null)
				 	{
				 		page.parent = this
				 	}
				 	var newpage = this.get('pages').add(page,options);
				 }else{
				 	page ={parent:this};
				 	var newpage = this.get('pages').add(page,options);
				 }
				return newpage;//this returns the collection
			},
			getPages:function(n)
			{
				return this.get('pages').models;
			},
			showPage:function(n)
			{
				this.getPages()[n].set({displayed:true});
			},
			showPageById:function(id)
			{
				// I need to traverse as deep as I can to find a page with that name
				//var pageObj = this.get('pages').where({el:id});	
				//You can set up the Analytics here
				var pageObj = this.getPageById(id)
				if(pageObj.length!=0){
					pageObj[0].set({displayed:true});
				}
			},
			getPageById:function(id)
			{
				var res = [];
				var currentSearchObj = this
				var findme =  function(obj,id)
				{	
					var fobj = obj.get('pages').where({el:id});	
					if(fobj.length==0)
					{
						for(var i=0;i<obj.get('pages').length;i++)
						{
							if(obj.getPages()[i].get('pageController')!=null)
							{
								currentSearchObj = obj.getPages()[i].get('pageController');
								findme(currentSearchObj,id);
							}
						}
					}
					else
					{
						res.push(fobj[0]);			
					}
				}		
				findme(currentSearchObj,id);
				return res;
	
			}
	
		});
	}
);