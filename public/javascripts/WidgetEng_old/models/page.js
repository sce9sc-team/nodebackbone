define(['zepto','underscore','backbone','backboneAssociations'],
function($,_, Backbone)
{
	
		
		WidgetEng.Page = Backbone.AssociatedModel.extend({
			relations: [
		        {
		            type: Backbone.One, //nature of the relationship
		            key: 'pageController', 
		            relatedModel: 'WidgetEng.PageController' //AssociatedModel for attribute key
		        }
		    ],	
			defaults:{
				tagName:'div',
				classname:"page",
				el:"testPage",
				width:"100%",
				height:"100%",
				parent:null,	
				pageController:null,
				html:'testPage',		
				copy:false,
				displayed:false
			},
			initialize: function(){
				console.log('a new Page was created');
				//View initialization
				//Creating a view
				this.on("error", function(model, error){
					console.log(error); 
				}); 

				this.on("destroy",function(){alert('destroy ')})

				this.on('change:pageController',function(page){
					//This the  Event for adding a controller to a page
					console.log('---fire change:pageController-----')
					this.get('pageController').addView();
				});			
				this.on('change:displayed',function(page){
					//get the controller 

					if(this.get('displayed'))
					{

						if(this.get('parent').get('displayedPage')==null){				
							this.get('parent').set({'displayedPage':this},{silent: true});				
						}else
						{				
							this.get('parent').get('displayedPage').set({displayed:false});				
							this.get('parent').set({'displayedPage':this},{silent: true});	
						}

						if(this.get('parent').get('parent')!=null)
						{
							this.get('parent').get('parent').set({displayed:true});					
						}
					}

				});


			},

			addView:function()
			{

				this.view = new WidgetEng.PageView({
					model:this,
					tagName: this.get('tagName'),
					className: this.get('className'),
					id:this.get('el'),
					html:this.get('html'),
					parent_el:$("#"+this.get('parent').get('el'))});
				var id = this.get('el');
			},

			addController:function()
			{
				//You can only add one controller on a page
				if(this.get('pageController')==null){
					console.log('-----------Adding a controller---------------');			
					var newPageController = new WidgetEng.PageController({el:this.get('el')+'Controller',parent:this})
					this.set({'pageController':newPageController});
					return newPageController;//this returns the collection
				}
			},	
			copyPage:function(page)
			{
				var copyObj = this.clone();
				copiedObjects.push(copyObj);
				return copyObj;

			},
			movePage:function()
			{
				this.clone();
			},
			getController:function()
			{
				return this.get('pageController');
			},
			setHtml:function(data)
			{
				this.set({html:data})
			}



		});
		
		return WidgetEng.Page;
	}
);