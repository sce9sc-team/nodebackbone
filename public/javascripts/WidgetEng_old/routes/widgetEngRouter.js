define(['zepto','underscore','backbone','backboneAssociations'],
function($,_, Backbone)
{
	
		WidgetEng.WidgetEngRouter = Backbone.Router.extend({
			routes: {
			"home/" : "showHomePage",
			"about1/" : "showAbout1",
			"*other/" : "defaultRoute"		
			},		
			showAbout1: function(){WidgetEng.mainController.showPage(1)},				
			showHomePage:function(){
				WidgetEng.mainController.showPage(0);
			},
			defaultRoute: function(other){	
				WidgetEng.mainController.showPageById(other)
			}
		});
			
});