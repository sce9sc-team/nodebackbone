define(['zepto','underscore','backbone','backboneAssociations'],
function($,_, Backbone)
{
	
		WidgetEng.PageControllerView = Backbone.View.extend({
		
				events: {
		    	"touchstart .pageController":"swipeStartEvent",
		    	"touchmove .pageController":"swipeMoveEvent"
		    	},
		
				initialize: function(){
				  		console.log('start Page Controller View');	
						if(this.model.get('parent'))
						{
							$('#'+this.model.get('parent').get('el')).append(this.el); //append this controller el to the page el 					
						}	          
			            this.listenTo(this.model, "remove", this.remove); //remove the view when object is removed        
	   			
			            this.listenTo(this.model, "change:el", this.updateEl); //update el when the objects el has changed
			            this.render();
			    },
				render: function()
				{
					console.log(this.$el);	
					this.el.className = this.model.get('classname');
					this.el.style.width = this.model.get('width');
					this.el.style.height = this.model.get('height');
					this.el.style.background = "red"
					return this; //recommended as this enables calls to be chained.
				}, 
				updateEl:function(pageCont)
				{
					console.log('PageController updateEl event triggered');
					this.$el.attr('id',pageCont.get('el'))		
				},
				swipeStartEvent:function(e){
					//console.log(e)
				},
				swipeMoveEvent:function(e)
				{
					//console.log(e)
				}
			});
		
			
});
