define(['zepto','underscore','backbone','backboneAssociations'],
function($,_, Backbone)
{
	
		if(typeof WidgetEng == 'undefined')WidgetEng = {};
		WidgetEng.PageView = Backbone.View.extend({		
				initialize: function(){ 
						console.log('Initialize Page View'); 
						if(this.options.parent_el.children().length == 0)
						{
							this.options.parent_el.append(this.el)
						}
						else{
							var index = _.indexOf(this.model.get('parent').getPages(), this.model);
							$(this.options.parent_el.children()[index-1]).after(this.el);
						}	 
						//append the element on the parent
			            this.listenTo(this.model, "remove", this.remove); // Listener to remove the view when the page obj is removed
			          	this.listenTo(this.model, "change:el", this.updateEl);
			            this.listenTo(this.model, "change:html", this.updateHtml);
			            this.listenTo(this.model, "change:displayed", this.showPage);
	            
	            
			            this.render();
			    },
				render: function()
				{
					console.log("Start Rendering the page");
					this.el.className = this.model.get('classname');
					this.el.style.width = this.model.get('width');
					this.el.style.height = this.model.get('height');
					this.el.innerHTML = this.options.html;
					this.el.style.background = "gray"
					return this; //recommended as this enables calls to be chained.
				}, 
				updateEl:function(page){
					console.log(page.get('el'));
					this.$el.attr('id',page.get('el'))
				},
				updateHtml:function(page){
					this.options.html = page.get('html');
					this.el.innerHTML = this.options.html;
					//this.render;
				},
				showPage:function()
				{
					if(this.model.get('displayed')==true){
						this.el.className = this.model.get('classname') +' '+ 'active';
					}
					else{
						this.el.className = this.model.get('classname');
					}		
				},
		
			});
});