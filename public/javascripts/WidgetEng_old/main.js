var WidgetEng = {};

require.config({
 	paths: {
    	zepto: 'lib/zepto',
    	underscore: 'lib/underscore',
    	backbone: 'lib/backbone',
 		backboneAssociations: 'lib/backbone-associations-min',
		io:'/socket.io/socket.io',	
  	},
  	shim:{
		zepto:{
			exports: '$'
		},
		underscore:{
			exports: '_'
		},
		backbone:{
			deps: ["underscore", "zepto"],
			exports: "Backbone"
		},
		backboneAssociations: {
	        deps: ['backbone'],
			exports: 'Backbone.AssociatedModel'
	    },
		io:{
			exports: 'io'
		}
	}

});
require([
  // Load our app module and pass it to our definition function
  //'app',
	'io',
   	'models/page',
	'models/pageController',
	'views/pageView',
	'views/pageControllerView',
	'routes/widgetEngRouter'	
], function(){
 	
/*	WidgetEng.socket = io.connect('http://localhost:3000');	
	WidgetEng.widgetEngRouter = new WidgetEng.WidgetEngRouter();
	WidgetEng.mainController = new WidgetEng.PageController({el:"main"});

	WidgetEng.mainController.addPage({el:'newPage',html:'<div>This is a new page</div>'})
*/	
	//html5sql.openDatabase("demo", "Demo Database", 5*1024*1024);
	//html5sql.process([pages_Table_Statement,controllerpages_Table_Statement,controller_Table_Statement],function(){alert(1)},function(){alert(2)})
	
	//test.mainController.addPage({el:'test1',html:'11111111111111'})
	//test.mainController.addPage({el:'test2',html:'22222222'})
	//find(test.mainController,'test1');
/*	
	Backbone.history.start();	
	WidgetEng.widgetEngRouter.navigate('home/',{trigger: true});
*/
	
});





/*script( src="/javascripts/zepto.js" )
script( src="/javascripts/underscore.js" )
script( src="/javascripts/handlebars-1.0.rc.1.js" )
script( src="/javascripts/backbone.js" )
script( src="/javascripts/backbone-associations.js")
script( src="/socket.io/socket.io.js")
script( src="/javascripts/WidgetEng.js")
script( src="/javascripts/init.js")
script( src="/javascripts/html5sql.js")

*/