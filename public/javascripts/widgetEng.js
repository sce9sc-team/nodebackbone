var WidgetEng = {}

WidgetEng.socket = io.connect('http://localhost:3000');	


var copiedObjects = [];

Backbone.Model.prototype._super = function(method){
  return this.constructor.__super__[method].apply(this, _.rest(arguments));
};




WidgetEng.ParseDom = function(id)
{
	var ObjectsModel = {widgets:[]}
	function getChildren(w,current){
		var wgts = $(w).children('[data-widget]');
		if(wgts.length>0)
		{
			for(var i=0;i<wgts.length;i++)
			{
				var oo = {}
				oo.widgets = [];
				oo.el = wgts[i].id;
				oo.width = 	wgts[i].style.width;
				oo.parentw = current;
				current.widgets.push(oo);				
				getChildren(wgts[i],oo);				
			}			
		}		
	};
	getChildren($('#'+id)[0],ObjectsModel);
	return ObjectsModel;
}

WidgetEng.WidgetView = Backbone.View.extend({	
	
		initialize: function(){
		  		console.log('initializing Widget');	
				if(this.model.get('parent'))
				{
					$('#'+this.model.get('parent').get('el')).append(this.el); //append this controller el to the page el 					
				}	          
	            this.listenTo(this.model, "remove", this.remove); //remove the view when object is removed        
	   			
	            this.listenTo(this.model, "change:el", this.updateEl); //update el when the objects el has changed
				this.listenTo(this.model, "change:background", function(){this.el.style.background = this.model.get('background') });
				this.listenTo(this.model, "change:width", function(){this.el.style.width = this.model.get('width') });
				this.listenTo(this.model, "change:height", function(){this.el.style.height = this.model.get('height') });
				this.listenTo(this.model, "change:border", function(){this.el.style.border = this.model.get('border') });
				this.listenTo(this.model, "change:color", function(){this.el.style.color = this.model.get('color') });
				this.listenTo(this.model, "change:events", this.eventsChange);
				
				//this.listenTo(this.model, "change:events", function(model){
				//	console.log(model.get('events'))
				//});
				this.render();
	    },
		render: function()
		{
			console.log(this.$el);	
			this.el.className = this.model.get('classname');
			this.el.style.width = this.model.get('width');
			this.el.style.height = this.model.get('height');
			this.el.style.border = this.model.get('border');
			this.el.style.background = this.model.get('background') ||"white";
			this.el.dataset.selectable = "true";
			this.el.dataset.widget = this.model.get('classname');
			return this; //recommended as this enables calls to be chained.
		}, 
		updateEl:function(widget)
		{
			console.log('PageController updateEl event triggered');
			this.$el.attr('id',widget.get('el'))		
		},
		eventsChange:function(wgt)
		{
			var prevEvents = wgt.get('prevEvents');
			for(var i=0;i<prevEvents.length;i++)
			{
				delete this[prevEvents[i][_.keys(prevEvents[i])[0]]];
			}
			
			this.undelegateEvents();
			var events = {};
			for(var i=0;i<wgt.changed.events.length;i++)
			{
				var eventName = _.values(wgt.changed.events[i])[0];
				var eventkey = _.keys(wgt.changed.events[i])[0];
				this[eventName] = WidgetEng.customEvents[eventName];
				events[eventkey] = eventName;
			}
			this.delegateEvents(events);
		},
		fireCustomEvent:function(eventName)
		{
			var eventsArray = Object.keys(WidgetEng.customEvents);
			if(eventsArray.indexOf(eventName)>-1)
			{
				WidgetEng.customEvents[eventName]();
			}
		}
		
	});
	


WidgetEng.Widget = Backbone.AssociatedModel.extend({
	 relations: [
	 {
                type:Backbone.Many,
                key:'widgets',
                relatedModel:'WidgetEng.Widget'
     }],
	
	defaults:{		
		tagName:'div',
		classname:"widget",	
		el:"widget",
		width:"100%",
		height:"100%",	
		border:"",
		background:'red',
		color:"white",
		widgets:[],
		parent:null,
		displayedPage:null,
		viewClass:'WidgetView',
		selectable:false,
		events:[],
		prevEvents:[]
	},
	initialize:function()
	{	
		this.addInitialView();
		this.historyChanges=[];
		this.currentHistoryIndex=0;
		this.undoFlag = false;
		this.on("add:widgets", function(widget){	
			if(widget.get('parent')==null)
			{
				widget.set({parent:this})
			}
			console.log(widget);
			widget.addView(widget);					
		});
		this.on('change',function(){
			if(!this.undoFlag){
				var newChanges = _.clone(this._currentAttributes)
				this.historyChanges.push(newChanges);
				this.currentHistoryIndex = this.historyChanges.length;
			}
		//this.on('change:events',function(){console.log(1)})
			
		})
	},
	addEventsToView:function(e){
		console.log(e)
	},
	addInitialView:function(){
		if(this.get('el')=='main'){	
			this.view = new WidgetEng[this.get("viewClass")]({el:$("#"+this.get('el')),model:this});			
		}	
	},
	addView:function()
	{
		this.view = new WidgetEng[this.get("viewClass")]({
				model:this,
				tagName: this.get('tagName'),
				className: this.get('classname'),
				id:this.get('el'),
				parent_el:$("#"+this.get('parent').get('el'))
			});
	},
	getWidgets:function(n)
	{
		return this.get(this.relations[0].key).models;
	},
	addWidget:function(widget,options)
	{
		 if(widget!=null)
		 {
		 	//if page not null aka you have passed params
		 	//add parent to params.
		 	//need to check if object model 
		 	if(widget.attributes==null)
		 	{
		 		widget.parent = this
		 	}
		 	var newWidget = this.get(this.relations[0].key).add(widget,options);
		 }else{
		 	widget={parent:this};
		 	var newWidget = this.get(this.relations[0].key).add(widget,options);
		 }
		return newWidget;//this returns the collection
	},
	getWidgetById:function(id)
	{
		var res = [];
		var currentSearchObj = this
		var findme =  function(obj,id)
		{	
			var fobj = obj.get(currentSearchObj.relations[0].key).where({el:id});	
			if(fobj.length==0)
			{
				if(obj.get(currentSearchObj.relations[0].key).length!=0)
				{
					for(var i=0;i<obj.get(currentSearchObj.relations[0].key).length;i++)
					{
						if(obj.getWidgets()[i].get(currentSearchObj.relations[0].key)!=null)
						{
							currentSearchObj = obj.getWidgets()[i]
							findme(currentSearchObj,id);
						}
					}
				}
			}
			else
			{
				res.push(fobj[0]);			
			}
		}		
		findme(currentSearchObj,id);
		return res;	
	},
	undo:function(){
		this.undoFlag = true;
		
		this.currentHistoryIndex=this.currentHistoryIndex-1;
		
		var changes = this.historyChanges[this.currentHistoryIndex-1];
		console.log(changes)
		this.set(changes);
		this.undoFlag = false;
	},
	removeEvents:function(ev){
		var currentEvents = this.get('events').slice();
		var prevEvents = currentEvents.slice();
		_.each(currentEvents, function(e,i,l){
			if(_.keys(e)[0]==_.keys(ev)[0])
			{
				if(_.values(e)[0]==_.values(ev)[0])
				{
					currentEvents.splice(i,1);
				}
			}
		});
		
		this.set({prevEvents:prevEvents});
		this.set({events:currentEvents});	
	},
	addEvents:function(ev){
		var newevents =[];
		var update = false;
		var currentEvents = this.get('events');
		_.each(currentEvents, function(e,i,l){
			if(!update){
				if(_.keys(e)[0]==_.keys(ev)[0])
				{
					e[_.keys(e)[0]] = ev[_.keys(ev)[0]];					
					newevents.push(e);
					update = true;
				}
				else{
					newevents.push(e);
				}
			}
		})
		if(!update)
		{
			newevents.push(ev);
		}
		this.set({prevEvents:currentEvents});
		this.set({events:newevents});		
	}
	
});


WidgetEng.SiteView = WidgetEng.WidgetView.extend({
	events: {
	"click":"setCurrentWidget",
	"mousemove":"moveResizeEvent",
	"mousedown":"startResizeEvent",
	"mouseup .site":"stopResizeEvent",
	"mouseover .site":"hoverElements",
	},
	editable:function(e){
			e.target.setAttribute("contenteditable","true");
			e.target.focus();
	},
	getCurrentSelectedWidget:function(w){
		console.log('-------')
		console.log(w)
		var res = null;
		function find(wgt){
			if(wgt.dataset.selectable){
				if(wgt.dataset.selectable=="false"){
					find(wgt.parentNode)
				}else{
				  res=wgt;
				}
			}
			else{find(wgt.parentNode)}		
		}
		find(w);
		return res
	},
	setCurrentWidget:function(e)
	{
		var w = this.getCurrentSelectedWidget(e.target);
		
		if(e.target.className != 'handles'){
		if(w.id == this.el.id)
		{
				if(this.model.get('selectable'))
				{
					if(this.model.get('currentSelectedWidget')!=this.model)
					{
						var currentWidgt = this.model.get('currentSelectedWidget');
						this.model.set({'currentSelectedWidget':this.model});
						if(currentWidgt.get('handles'))
						{
							currentWidgt.set({handles:false});
						}							
					}else
					{
						this.model.set({'currentSelectedWidget':this.model});
					}
				}
		}
		else
		{
			var wigt = this.model.getWidgetById(w.id)[0];
			if(wigt.get('selectable'))
			{
					if(this.model.get('currentSelectedWidget')==wigt)
					{
						//unset
						this.model.set({'currentSelectedWidget':this.model});
						wigt.set({handles:false});
					}else{
						//set
						if(this.model.get('currentSelectedWidget'))
						{
							var curWigt = this.model.get('currentSelectedWidget');
							curWigt.set({handles:false});
						}
						this.model.set({'currentSelectedWidget':wigt});
						wigt.set({handles:true});
					
					}
				
			}
		}}
	},
	moveResizeEvent:function(e)
	{	
		var targetWidget = this.model.get('currentSelectedWidget');
		if(targetWidget){
			if(targetWidget.get('canResize'))
			{
				switch(this.options.resizedir)
				{
					case 'w':
						var parentWidth = parseInt(window.getComputedStyle(targetWidget.get('parent').view.el,null).getPropertyValue("width").replace("px",''));
						var width = parseInt(targetWidget.get('width').replace("px",''));			
						var newWidth = width+(e.pageX-this.options.pageX)
					//	if(newWidth<=parentWidth){				
							targetWidget.set({width:newWidth+'px'});
							this.options.pageX = e.pageX;					
					//	}	
						break;
					case 'h':
						var parentHeight= parseInt(window.getComputedStyle(targetWidget.get('parent').view.el,null).getPropertyValue("height").replace("px",''));
						var height = parseInt(targetWidget.get('height').replace("px",''));			
						var newHeight = height+(e.pageY-this.options.pageY)
						//if(newHeight<=parentHeight){				
							targetWidget.set({height:newHeight+'px'});
							this.options.pageY = e.pageY;					
						//}
						break;
				}	
			}
		}
	},
	hoverElements:function(e)
	{
		//show widgets you are on top
	},
	startResizeEvent:function(e)
	{
		if(e.target.className == 'handles'){
			this.options.pageX = e.pageX;
			this.options.pageY = e.pageY;
			this.model.get('currentSelectedWidget').set({'canResize':true})
			
			switch(e.target.id)
			{
				case 'tst':
					this.options.resizedir ='w';
					break;
				case 's1':
					this.options.resizedir ='h';
					break;
			 	default:
					this.options.resizedir ='h';
					break;
			}
		}
		
	},	
	stopResizeEvent:function(e)
	{
		if(this.model.get('currentSelectedWidget')){
		if(this.model.get('currentSelectedWidget').get('canResize'))
		{
			this.model.get('currentSelectedWidget').set({'canResize':false})
		}}
	},

});


WidgetEng.Site = WidgetEng.Widget.extend({
	defaults:_.extend({},WidgetEng.Widget.prototype.defaults,{
		classname:"site",
		el:"site",
		viewClass: "SiteView"
	}),
	addInitialView:function(){
		if(this.get('el')=='site'){	
			this.view = new WidgetEng[this.get("viewClass")]({el:$("#"+this.get('el')),model:this});			
		}	
	},
});


WidgetEng.BoxView = WidgetEng.WidgetView.extend({

	render: function()
	{
		console.log(this.$el);	
		this.el.className = this.model.get('classname');
		this.el.style.width = this.model.get('width');
		this.el.style.height = this.model.get('height');
		this.el.style.background = this.model.get('background');
		this.el.style.border = this.model.get('border');
		this.el.style.position ="relative";
		this.el.style.overflow = 'hidden';
		this.el.style.boxSizing="border-box";
		this.el.style.MozBoxSizing="border-box";
		this.el.style.webkitBoxSizing="border-box";
		this.el.dataset.widget = this.model.get('classname');
		this.addHandles();
		this.postRender();
		
		return this; //recommended as this enables calls to be chained.
		
				
	},
	postRender:function()
	{
		this.el.dataset.selectable = this.model.get('selectable');
		this.listenTo(this.model, "change:selectable", function(){this.el.dataset.selectable = this.model.get('selectable') });
	},
	showHandles:function(){
		for(var i=0;i<this.handles.length;i++)
		{
			this.handles[i].style.display = "block";
		}
	},
	hideHandles:function(){
		for(var i=0;i<this.handles.length;i++)
		{
			this.handles[i].style.display = "none";
		}
	},
	addHandles:function()
	{
		this.handles =[];
		var fragment = document.createDocumentFragment();
		var b1 = document.createElement('div');
		b1.className="handles";
		b1.style.cssText = 'z-index:98;display:none;position:absolute;left:100%;top:0px;width:1px; margin-left:-1px; height:100%; border:1px dashed yellow;box-sizing: border-box; ';
		b1.dataset.selectable = "false";
		this.handles.push(b1);
		
		var b2 = document.createElement('div')
		b2.className="handles"
		b2.style.cssText = 'z-index:98;display:none;position:absolute;left:-1px;top:0px;width:1px; height:100%; border:1px dashed yellow;box-sizing: border-box; ';
		b2.dataset.selectable = "false";
		this.handles.push(b2);
		
		var b3 = document.createElement('div')
		b3.className="handles"
		b3.style.cssText = 'z-index:98;display:none;position:absolute;top:-1px;height:1px; width:100%; border:1px dashed yellow;box-sizing: border-box; ';
		b3.dataset.selectable = "false";
		this.handles.push(b3);
		
		var b4 = document.createElement('div')
		b4.className="handles"
		b4.style.cssText = 'z-index:98;display:none;position:absolute;bottom:-1px;width:100%; height:1%; border:1px dashed yellow;box-sizing: border-box; ';
		b4.dataset.selectable = "false";
		this.handles.push(b4);
		
		var a1 = document.createElement('div')
		a1.className="handles"
		a1.style.cssText = 'z-index:99;display:none;position:absolute;top:-1px;left:-1px;width: 10px; height: 10px; background-color: white; border: 1px solid black; box-sizing: border-box; ';
		a1.dataset.selectable = "false";
		this.handles.push(a1);
		
		var a2 = document.createElement('div')
		a2.className="handles"
		a2.style.cssText = 'z-index:99;display:none;position:absolute;top:-1px;margin-left:-5px;left:50%;width: 10px; height: 10px; background-color: white; border: 1px solid black; box-sizing: border-box; ';
		a2.dataset.selectable = "false";
		this.handles.push(a2);
		
		var a3 = document.createElement('div')
		a3.className="handles"
		a3.style.cssText = 'z-index:99;display:none;position:absolute;top:-1px;right:-1px;width: 10px; height: 10px; background-color: white; border: 1px solid black; box-sizing: border-box; ';
		a3.dataset.selectable = "false";
		this.handles.push(a3);
		
		var a4 = document.createElement('div')
		a4.className="handles"
		a4.id="tst"
		a4.style.cssText = 'z-index:99;display:none;position:absolute;margin-top:-5px;top:50%;right:-1px;width: 10px; height: 10px; background-color: white; border: 1px solid black; box-sizing: border-box; ';
		a4.dataset.selectable = "false";
		this.handles.push(a4);
		
		var a5 = document.createElement('div')
		a5.className="handles"
		a5.style.cssText = 'z-index:99;display:none;position:absolute;bottom:-1px;left:-1px;width: 10px; height: 10px; background-color: blue; border: 1px solid black; box-sizing: border-box; ';
		a5.dataset.selectable = "false";
		this.handles.push(a5);
		
		var a6 = document.createElement('div')
		a6.className="handles"
		a6.style.cssText = 'z-index:99;display:none;position:absolute;top:50%;margin-top:-5px;left:-1px;width: 10px; height: 10px; background-color: blue; border: 1px solid black; box-sizing: border-box; ';
		a6.dataset.selectable = "false";
		this.handles.push(a6);
		
		var a7 = document.createElement('div')
		a7.className="handles"
		a7.id="s1"
		a7.style.cssText = 'z-index:99;display:none;position:absolute;bottom:-1px;margin-left:-5px;left:50%;width: 10px; height: 10px; background-color: white; border: 1px solid black; box-sizing: border-box; ';
		a7.dataset.selectable = "false";
		this.handles.push(a7);
			
		var a8 = document.createElement('div')
		a8.className="handles"
		a8.style.cssText = 'z-index:99;display:none;position:absolute;bottom:-1px;right:-1px;width: 10px; height: 10px; background-color: blue; border: 1px solid black; box-sizing: border-box; ';
		a8.dataset.selectable = "false";
		this.handles.push(a8);	
		
		fragment.appendChild(b1);
		fragment.appendChild(b2);
		fragment.appendChild(b3);
		fragment.appendChild(b4);
		fragment.appendChild(a1);
		fragment.appendChild(a2);
		fragment.appendChild(a3);
		fragment.appendChild(a4);
		fragment.appendChild(a5);
		fragment.appendChild(a6);
		fragment.appendChild(a7);
		fragment.appendChild(a8);
		
		this.el.appendChild(fragment);
		
		this.listenTo(this.model, "change:handles", function(){
			if(this.model.get('handles'))
			{
				this.showHandles();
			}else
			{
				this.hideHandles();
			}
		});
		
	
		
	}
});


WidgetEng.Box = WidgetEng.Widget.extend({
	defaults:_.extend({},WidgetEng.Widget.prototype.defaults,{
		classname:"box",
		el:"box",
		viewClass: "BoxView",
		background:"blue",
		border:"1px solid black",	
		handles:false,
		canResize:false,
		selectable:true
	})
});

WidgetEng.LabelView  = WidgetEng.BoxView.extend({
	template: _.template('<span id="<%= id %>" data-selectable="false" style="width:100%;height:100%"><%= text %></span>'),
	postRender:function()
	{
		var html = this.template({id:'test',text:this.model.get('text')});
		var a = document.createElement('div');
		a.style.width="100%";
		a.style.height="100%";
		a.style.background="gray";
		a.dataset.selectable = "false";
		a.style.position= "relative";
		a.setAttribute('contenteditable','true');
		a.innerHTML = this.model.get('text');	
		a.style.overflow = "hidden";
		a.className ="text_selectable"			
		this.el.appendChild(a)
		//this.$el.html(this.el.innerHTML+html);
		//this._super('s);
		this.el.dataset.selectable = this.model.get('selectable');
		this.listenTo(this.model, "change:selectable", function(){this.el.dataset.selectable = this.model.get('selectable') });
	}
	
	
});


WidgetEng.Label = WidgetEng.Widget.extend({
	relations: [],
	defaults:{
		tagName:'div',	
		width:"100%",
		height:"100%",	
		parent:null,
		displayedPage:null,			
		events:[],		
		classname:"Label",
		el:"Label",
		viewClass: "LabelView",
		background:"blue",
		border:"1px solid black",
		text:"",
		selectable:true,	
		handles:false,
		canResize:false,	
	}
});


WidgetEng.vBoxView  = WidgetEng.BoxView.extend({
	render: function()
	{
		console.log(this.$el);	
		this.el.className = this.model.get('classname');
		this.el.style.width = this.model.get('width');
		this.el.style.height = this.model.get('height');
		this.el.style.background = this.model.get('background');
		this.el.style.border = this.model.get('border');
		this.el.style.position ="relative";
		this.el.style.boxSizing="border-box";
		this.el.style.MozBoxSizing="border-box";
		this.el.style.webkitBoxSizing="border-box";
		this.addHandles();
		this.postRender();
		
		return this; //recommended as this enables calls to be chained.
		
				
	}
	
	
});

WidgetEng.hBoxView  = WidgetEng.BoxView.extend({
	
});

WidgetEng.hBox = WidgetEng.Box.extend({
	relations:[
	 {
                type:Backbone.Many,
                key:'widgets',
                relatedModel:'WidgetEng.Box'
     }],
	defaults:_.extend({},WidgetEng.Widget.prototype.defaults,{
		classname:"hbox",
		el:"hbox",
		viewClass: "hBoxView",
		background:"blue",
		border:"1px solid black",
		handles:false,
		canResize:false,
		selectable:true
	})
});

WidgetEng.vBox = WidgetEng.Widget.extend({
	relations:[
	 {
                type:Backbone.Many,
                key:'widgets',
                relatedModel:'WidgetEng.Box'
     }],
	defaults:_.extend({},WidgetEng.Widget.prototype.defaults,{
		classname:"vbox",
		el:"vbox",
		viewClass: "vBoxView",
		background:"blue",
		border:"1px solid black",
		handles:false,
		canResize:false,
		selectable:true
	})
});





WidgetEng.PageControllerView = Backbone.View.extend({
		
		events: {
    	"touchstart .pageController":"swipeStartEvent",
    	"touchmove .pageController":"swipeMoveEvent"
    	},
		
		initialize: function(){
		  		console.log('start Page Controller View');	
				if(this.model.get('parent'))
				{
					$('#'+this.model.get('parent').get('el')).append(this.el); //append this controller el to the page el 					
				}	          
	            this.listenTo(this.model, "remove", this.remove); //remove the view when object is removed        
	   			
	            this.listenTo(this.model, "change:el", this.updateEl); //update el when the objects el has changed
	            this.render();
	    },
		render: function()
		{
			console.log(this.$el);	
			this.el.className = this.model.get('classname');
			this.el.style.width = this.model.get('width');
			this.el.style.height = this.model.get('height');
			this.el.style.background = this.model.get('background');
			return this; //recommended as this enables calls to be chained.
		}, 
		updateEl:function(pageCont)
		{
			console.log('PageController updateEl event triggered');
			this.$el.attr('id',pageCont.get('el'))		
		},
		swipeStartEvent:function(e){
			//console.log(e)
		},
		swipeMoveEvent:function(e)
		{
			//console.log(e)
		}
	});
	
WidgetEng.PageView = Backbone.View.extend({		
		initialize: function(){ 
				console.log('Initialize Page View'); 
				if(this.options.parent_el.children().length == 0)
				{
					this.options.parent_el.append(this.el)
				}
				else{
					var index = _.indexOf(this.model.get('parent').getPages(), this.model);
					$(this.options.parent_el.children()[index-1]).after(this.el);
				}	 
				//append the element on the parent
	            this.listenTo(this.model, "remove", this.remove); // Listener to remove the view when the page obj is removed
	          	this.listenTo(this.model, "change:el", this.updateEl);
	            this.listenTo(this.model, "change:html", this.updateHtml);
	            this.listenTo(this.model, "change:displayed", this.showPage);
	            
	            
	            this.render();
	    },
		render: function()
		{
			console.log("Start Rendering the page");
			this.el.className = this.model.get('classname');
			this.el.style.width = this.model.get('width');
			this.el.style.height = this.model.get('height');
			this.el.innerHTML = this.options.html;
			this.el.style.background = "gray"
			return this; //recommended as this enables calls to be chained.
		}, 
		updateEl:function(page){
			console.log(page.get('el'));
			this.$el.attr('id',page.get('el'))
		},
		updateHtml:function(page){
			this.options.html = page.get('html');
			this.el.innerHTML = this.options.html;
			//this.render;
		},
		showPage:function()
		{
			if(this.model.get('displayed')==true){
				this.el.className = this.model.get('classname') +' '+ 'active';
			}
			else{
				this.el.className = this.model.get('classname');
			}		
		},
		
	});




WidgetEng.Page = Backbone.AssociatedModel.extend({
	relations: [
        {
            type: Backbone.One, //nature of the relationship
            key: 'pageController', 
            relatedModel: 'PageController' //AssociatedModel for attribute key
        }
    ],	
	defaults:{
		tagName:'div',
		classname:"page",
		el:"testPage",
		width:"100%",
		height:"100%",
		parent:null,	
		pageController:null,
		html:'testPage',		
		copy:false,
		displayed:false
	},
	initialize: function(){
		console.log('a new Page was created');
		//View initialization
		//Creating a view
		this.on("error", function(model, error){
			console.log(error); 
		}); 
		
		this.on("destroy",function(){alert('destroy ')})
		
		this.on('change:pageController',function(page){
			//This the  Event for adding a controller to a page
			console.log('---fire change:pageController-----')
			this.get('pageController').addView();
		});			
		this.on('change:displayed',function(page){
			//get the controller 
					
			if(this.get('displayed'))
			{
			
				if(this.get('parent').get('displayedPage')==null){				
					this.get('parent').set({'displayedPage':this},{silent: true});				
				}else
				{				
					this.get('parent').get('displayedPage').set({displayed:false});				
					this.get('parent').set({'displayedPage':this},{silent: true});	
				}
			
				if(this.get('parent').get('parent')!=null)
				{
					this.get('parent').get('parent').set({displayed:true});					
				}
			}			
		});
		
		
	},
	
	addView:function()
	{
		
		this.view = new WidgetEng.PageView({
			model:this,
			tagName: this.get('tagName'),
			className: this.get('className'),
			id:this.get('el'),
			html:this.get('html'),
			parent_el:$("#"+this.get('parent').get('el'))});
		var id = this.get('el');
	},
	
	addController:function()
	{
		//You can only add one controller on a page
		if(this.get('pageController')==null){
			console.log('-----------Adding a controller---------------');			
			var newPageController = new WidgetEng.PageController({el:this.get('el')+'Controller',parent:this})
			this.set({'pageController':newPageController});
			return newPageController;//this returns the collection
		}
	},	
	copyPage:function(page)
	{
		var copyObj = this.clone();
		copiedObjects.push(copyObj);
		return copyObj;
	
	},
	movePage:function()
	{
		this.clone();
	},
	getController:function()
	{
		return this.get('pageController');
	},
	setHtml:function(data)
	{
		this.set({html:data})
	}
	
	

});

WidgetEng.PageController = Backbone.AssociatedModel.extend({
	 relations: [
	 	{
                type:Backbone.Many,
                key:'pages',
                relatedModel:WidgetEng.Page
     }],

	url:'/api/test',
	defaults:{		
		tagName:'div',
		classname:"pageController",	
		el:"testController",
		width:"100%",
		height:"100%",	
		pages:[],
		parent:null,
		displayedPage:null
	
	},
	socket_events:{
		"pages":"setPages"
	},
	initialize: function(){
		this.get('pages').url= this.url;
		console.log('Initializing a Page controller'); 		
		//Creating a View for the Controller, if main Controller then : the div already exists
		if(this.get('el')=='main'){	
			this.view = new WidgetEng.PageControllerView({el:$("#"+this.get('el')),model:this});			
		}
	 	this.on("error", function(model, error){
			console.log(error); 
		}); 
		this.on("add:pages", function(page){	
			if(page.get('parent')==null)
			{page.set({parent:this})}
			console.log(page);
			page.addView();					
		});
		this.on("change:displayedPage",function(){
			//this.get('displayedPage').set({'displayed':true});		
		})
		this.initSockets()
		
	},
	initSockets: function()
	{
		if (this.socket_events && _.size(this.socket_events) > 0) {
			this.delegateSocketEvents(this.socket_events);
		}	
	},
	delegateSocketEvents: function (events) {
		for (var key in events) 
		{
			var method = events[key];
			if (!_.isFunction(method)) {
				method = this[events[key]];
			}
			if (!method) {
				throw new Error('Method "' + events[key] + '" does not exist');
			}
			method = _.bind(method, this);
			WidgetEng.socket.on(key, method);
	}
	},
	setPages:function(data)
	{
		console.log('socket get pages' );
		console.log(data);
		console.log(this);	
		this.get('pages').update(data.pages)
		this.showPage(0); // this is on order to set the view of the page // maybe it will be better to make an event 
		//or the above
		//test.myGalleryRouter.navigate('home/',{trigger: true});	
	},
	
	addView:function()
	{
		this.view = new WidgetEng.PageControllerView({
				model:this,
				tagName: this.get('tagName'),
				className: this.get('classname'),
				id:this.get('el'),
				parent_el:$("#"+this.get('parent').get('el'))
			});
	},
	addPage:function(page,options)
	{
		 if(page!=null)
		 {
		 	//if page not null aka you have passed params
		 	//add parent to params.
		 	//need to check if object model 
		 	if(page.attributes==null)
		 	{
		 		page.parent = this
		 	}
		 	var newpage = this.get(this.relations[0].key).add(page,options);
		 }else{
		 	page ={parent:this};
		 	var newpage = this.get(this.relations[0].key).add(page,options);
		 }
		return newpage;//this returns the collection
	},
	getPages:function(n)
	{
		return this.get('pages').models;
	},
	showPage:function(n)
	{
		this.getPages()[n].set({displayed:true});
	},
	showPageById:function(id)
	{
		// I need to traverse as deep as I can to find a page with that name
		//var pageObj = this.get('pages').where({el:id});	
		//You can set up the Analytics here
		var pageObj = this.getPageById(id)
		if(pageObj.length!=0){
			pageObj[0].set({displayed:true});
		}
	},
	getPageById:function(id)
	{
		var res = [];
		var currentSearchObj = this
		var findme =  function(obj,id)
		{	
			var fobj = obj.get(this.relations[0].key).where({el:id});	
			if(fobj.length==0)
			{
				for(var i=0;i<obj.get(this.relations[0].key).length;i++)
				{
					if(obj.getPages()[i].get('pageController')!=null)
					{
						currentSearchObj = obj.getPages()[i].get('pageController');
						findme(currentSearchObj,id);
					}
				}
			}
			else
			{
				res.push(fobj[0]);			
			}
		}		
		findme(currentSearchObj,id);
		return res;
	
	}
	
});


WidgetEng.WidgetEngRouter = Backbone.Router.extend({
/* define the route and function maps for this router */

	routes: {
	"home/" : "showHomePage",
	"about1/" : "showAbout1",
	/*Sample usage: http://unicorns.com/#about*/
	//"photos/:id" : "getPhoto",
	/*This is an example of using a ":param" variable which allows us to match any of the components between two URL slashes*/
	/*Sample usage: http://unicorns.com/#photos/5*/
	//"search/:query" : "searchPhotos",
	/*We can also define multiple routes that are bound to the same map function, in this case searchPhotos(). Note below how we're optionally passing in a reference to a page number if one is supplied*/
	/*Sample usage: http://unicorns.com/#search/lolcats*/
	//"search/:query/p:page" : "searchPhotos",
	/*As we can see, URLs may contain as many ":param"s as we wish*/ /*Sample usage: http://unicorns.com/#search/lolcats/p1*/
	//"photos/:id/download/*imagePath" : "downloadPhoto",
	/*This is an example of using a *splat. splats are able to match any number of
	URL components and can be combined with ":param"s*/
	/*Sample usage: http://unicorns.com/#photos/5/download/files/lolcat-car.jpg*/
	/*If you wish to use splats for anything beyond default routing, it's probably a good
	idea to leave them at the end of a URL otherwise you may need to apply regular expression parsing on your fragment*/
	"*other/" : "defaultRoute"
	/*This is a default route that also uses a *splat. Consider the
	default route a wildcard for URLs that are either not matched or where
	the user has incorrectly typed in a route path manually*/ /*Sample usage: http://unicorns.com/#anything*/
	},
	showAbout: function(){WidgetEng.mainController.showPage(0)},
	showAbout1: function(){WidgetEng.mainController.showPage(1)},
	getPhoto: function(id){
		/*
		Note that the id matched in the above route will be passed to this function
		*/
		console.log("You are trying to reach photo " + id); 
	},
	searchPhotos: function(query, page){
		var page_number = page || 1;
		console.log("Page number: " + page_number + " of the results for " + query);
	},
	downloadPhoto: function(id, path){ },
	showHomePage:function(){
		WidgetEng.mainController.showPage(0);
	},
	defaultRoute: function(other){	
		WidgetEng.mainController.showPageById(other)
	}
});

