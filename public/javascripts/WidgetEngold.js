var WidgetEng = {}
WidgetEng.socket = io.connect('http://localhost:3000');	


var copiedObjects = [];

Backbone.Model.prototype._super = function(method){
  return this.constructor.__super__[method].apply(this, _.rest(arguments));
}


WidgetEng.WidgetView = Backbone.View.extend({
	
		initialize: function(){
		  		console.log('initializing Widget');	
				if(this.model.get('parent'))
				{
					$('#'+this.model.get('parent').get('el')).append(this.el); //append this controller el to the page el 					
				}	          
	            this.listenTo(this.model, "remove", this.remove); //remove the view when object is removed        
	   			
	            this.listenTo(this.model, "change:el", this.updateEl); //update el when the objects el has changed
	            this.render();
	    },
		render: function()
		{
			console.log(this.$el);	
			this.el.className = this.model.get('classname');
			this.el.style.width = this.model.get('width');
			this.el.style.height = this.model.get('height');
			this.el.style.background = "red"
			return this; //recommended as this enables calls to be chained.
		}, 
		updateEl:function(widget)
		{
			console.log('PageController updateEl event triggered');
			this.$el.attr('id',widget.get('el'))		
		},
	
	});

WidgetEng.Widget = Backbone.AssociatedModel.extend({
	 relations: [
	 {
                type:Backbone.Many,
                key:'widgets',
                relatedModel:'WidgetEng.Widget'
     }],
	
	defaults:{		
		tagName:'div',
		classname:"widget",	
		el:"widget",
		width:"100%",
		height:"100%",	
		widgets:[],
		parent:null,
		displayedPage:null,
		viewClass:'WidgetView'

	},
	initialize:function()
	{
		if(this.get('el')=='main'){	
			this.view = new WidgetEng[this.get("viewClass")]({el:$("#"+this.get('el')),model:this});			
		}
		
		this.on("add:widgets", function(widget){	
			if(widget.get('parent')==null)
			{
				widget.set({parent:this})
			}
			console.log(widget);
			widget.addView(widget);					
		});
	},
	addView:function()
	{
		this.view = new WidgetEng[this.get("viewClass")]({
				model:this,
				tagName: this.get('tagName'),
				className: this.get('classname'),
				id:this.get('el'),
				parent_el:$("#"+this.get('parent').get('el'))
			});
	},
	getWidgets:function(n)
	{
		return this.get(this.relations[0].key).models;
	},
	addWidget:function(widget,options)
	{
		 if(widget!=null)
		 {
		 	//if page not null aka you have passed params
		 	//add parent to params.
		 	//need to check if object model 
		 	if(widget.attributes==null)
		 	{
		 		widget.parent = this
		 	}
		 	var newWidget = this.get(this.relations[0].key).add(widget,options);
		 }else{
		 	widget={parent:this};
		 	var newWidget = this.get(this.relations[0].key).add(widget,options);
		 }
		return newWidget;//this returns the collection
	},
	getWidgetById:function(id)
	{
		var res = [];
		var currentSearchObj = this
		var findme =  function(obj,id)
		{	
			var fobj = obj.get(this.relations[0].key).where({el:id});	
			if(fobj.length==0)
			{
				for(var i=0;i<obj.get(this.relations[0].key).length;i++)
				{
					if(obj.getWidgets()[i].get('pageController')!=null)
					{
						currentSearchObj = obj.getWidgets()[i].get('pageController');
						findme(currentSearchObj,id);
					}
				}
			}
			else
			{
				res.push(fobj[0]);			
			}
		}		
		findme(currentSearchObj,id);
		return res;	
	}
	
});

WidgetEng.Box = WidgetEng.Widget.extend({
	defaults:{		
		tagName:'div',
		classname:"widgetBox",	
		el:"widget",
		width:"100%",
		height:"100%",	
		widgets:[],
		parent:null,
		displayedPage:null
	}
});



WidgetEng.PageControllerView = Backbone.View.extend({
		
		events: {
    	"touchstart .pageController":"swipeStartEvent",
    	"touchmove .pageController":"swipeMoveEvent"
    	},
		
		initialize: function(){
		  		console.log('start Page Controller View');	
				if(this.model.get('parent'))
				{
					$('#'+this.model.get('parent').get('el')).append(this.el); //append this controller el to the page el 					
				}	          
	            this.listenTo(this.model, "remove", this.remove); //remove the view when object is removed        
	   			
	            this.listenTo(this.model, "change:el", this.updateEl); //update el when the objects el has changed
	            this.render();
	    },
		render: function()
		{
			console.log(this.$el);	
			this.el.className = this.model.get('classname');
			this.el.style.width = this.model.get('width');
			this.el.style.height = this.model.get('height');
			this.el.style.background = "red"
			return this; //recommended as this enables calls to be chained.
		}, 
		updateEl:function(pageCont)
		{
			console.log('PageController updateEl event triggered');
			this.$el.attr('id',pageCont.get('el'))		
		},
		swipeStartEvent:function(e){
			//console.log(e)
		},
		swipeMoveEvent:function(e)
		{
			//console.log(e)
		}
	});
	
WidgetEng.PageView = Backbone.View.extend({		
		initialize: function(){ 
				console.log('Initialize Page View'); 
				if(this.options.parent_el.children().length == 0)
				{
					this.options.parent_el.append(this.el)
				}
				else{
					var index = _.indexOf(this.model.get('parent').getPages(), this.model);
					$(this.options.parent_el.children()[index-1]).after(this.el);
				}	 
				//append the element on the parent
	            this.listenTo(this.model, "remove", this.remove); // Listener to remove the view when the page obj is removed
	          	this.listenTo(this.model, "change:el", this.updateEl);
	            this.listenTo(this.model, "change:html", this.updateHtml);
	            this.listenTo(this.model, "change:displayed", this.showPage);
	            
	            
	            this.render();
	    },
		render: function()
		{
			console.log("Start Rendering the page");
			this.el.className = this.model.get('classname');
			this.el.style.width = this.model.get('width');
			this.el.style.height = this.model.get('height');
			this.el.innerHTML = this.options.html;
			this.el.style.background = "gray"
			return this; //recommended as this enables calls to be chained.
		}, 
		updateEl:function(page){
			console.log(page.get('el'));
			this.$el.attr('id',page.get('el'))
		},
		updateHtml:function(page){
			this.options.html = page.get('html');
			this.el.innerHTML = this.options.html;
			//this.render;
		},
		showPage:function()
		{
			if(this.model.get('displayed')==true){
				this.el.className = this.model.get('classname') +' '+ 'active';
			}
			else{
				this.el.className = this.model.get('classname');
			}		
		},
		
	});




WidgetEng.Page = Backbone.AssociatedModel.extend({
	relations: [
        {
            type: Backbone.One, //nature of the relationship
            key: 'pageController', 
            relatedModel: 'PageController' //AssociatedModel for attribute key
        }
    ],	
	defaults:{
		tagName:'div',
		classname:"page",
		el:"testPage",
		width:"100%",
		height:"100%",
		parent:null,	
		pageController:null,
		html:'testPage',		
		copy:false,
		displayed:false
	},
	initialize: function(){
		console.log('a new Page was created');
		//View initialization
		//Creating a view
		this.on("error", function(model, error){
			console.log(error); 
		}); 
		
		this.on("destroy",function(){alert('destroy ')})
		
		this.on('change:pageController',function(page){
			//This the  Event for adding a controller to a page
			console.log('---fire change:pageController-----')
			this.get('pageController').addView();
		});			
		this.on('change:displayed',function(page){
			//get the controller 
					
			if(this.get('displayed'))
			{
			
				if(this.get('parent').get('displayedPage')==null){				
					this.get('parent').set({'displayedPage':this},{silent: true});				
				}else
				{				
					this.get('parent').get('displayedPage').set({displayed:false});				
					this.get('parent').set({'displayedPage':this},{silent: true});	
				}
			
				if(this.get('parent').get('parent')!=null)
				{
					this.get('parent').get('parent').set({displayed:true});					
				}
			}
			
		});
		
		
	},
	
	addView:function()
	{
		
		this.view = new WidgetEng.PageView({
			model:this,
			tagName: this.get('tagName'),
			className: this.get('className'),
			id:this.get('el'),
			html:this.get('html'),
			parent_el:$("#"+this.get('parent').get('el'))});
		var id = this.get('el');
	},
	
	addController:function()
	{
		//You can only add one controller on a page
		if(this.get('pageController')==null){
			console.log('-----------Adding a controller---------------');			
			var newPageController = new WidgetEng.PageController({el:this.get('el')+'Controller',parent:this})
			this.set({'pageController':newPageController});
			return newPageController;//this returns the collection
		}
	},	
	copyPage:function(page)
	{
		var copyObj = this.clone();
		copiedObjects.push(copyObj);
		return copyObj;
	
	},
	movePage:function()
	{
		this.clone();
	},
	getController:function()
	{
		return this.get('pageController');
	},
	setHtml:function(data)
	{
		this.set({html:data})
	}
	
	

});

WidgetEng.PageController = Backbone.AssociatedModel.extend({
	 relations: [
	 	{
                type:Backbone.Many,
                key:'pages',
                relatedModel:WidgetEng.Page
     }],

	url:'/api/test',
	defaults:{		
		tagName:'div',
		classname:"pageController",	
		el:"testController",
		width:"100%",
		height:"100%",	
		pages:[],
		parent:null,
		displayedPage:null
	
	},
	socket_events:{
		"pages":"setPages"
	},
	initialize: function(){
		this.get('pages').url= this.url;
		console.log('Initializing a Page controller'); 		
		//Creating a View for the Controller, if main Controller then : the div already exists
		if(this.get('el')=='main'){	
			this.view = new WidgetEng.PageControllerView({el:$("#"+this.get('el')),model:this});			
		}
	 	this.on("error", function(model, error){
			console.log(error); 
		}); 
		this.on("add:pages", function(page){	
			if(page.get('parent')==null)
			{page.set({parent:this})}
			console.log(page);
			page.addView();					
		});
		this.on("change:displayedPage",function(){
			//this.get('displayedPage').set({'displayed':true});		
		})
		this.initSockets()
		
	},
	initSockets: function()
	{
		if (this.socket_events && _.size(this.socket_events) > 0) {
			this.delegateSocketEvents(this.socket_events);
		}	
	},
	delegateSocketEvents: function (events) {
		for (var key in events) 
		{
			var method = events[key];
			if (!_.isFunction(method)) {
				method = this[events[key]];
			}
			if (!method) {
				throw new Error('Method "' + events[key] + '" does not exist');
			}
			method = _.bind(method, this);
			WidgetEng.socket.on(key, method);
	}
	},
	setPages:function(data)
	{
		console.log('socket get pages' );
		console.log(data);
		console.log(this);	
		this.get('pages').update(data.pages)
		this.showPage(0); // this is on order to set the view of the page // maybe it will be better to make an event 
		//or the above
		//test.myGalleryRouter.navigate('home/',{trigger: true});	
	},
	
	addView:function()
	{
		this.view = new WidgetEng.PageControllerView({
				model:this,
				tagName: this.get('tagName'),
				className: this.get('classname'),
				id:this.get('el'),
				parent_el:$("#"+this.get('parent').get('el'))
			});
	},
	addPage:function(page,options)
	{
		 if(page!=null)
		 {
		 	//if page not null aka you have passed params
		 	//add parent to params.
		 	//need to check if object model 
		 	if(page.attributes==null)
		 	{
		 		page.parent = this
		 	}
		 	var newpage = this.get(this.relations[0].key).add(page,options);
		 }else{
		 	page ={parent:this};
		 	var newpage = this.get(this.relations[0].key).add(page,options);
		 }
		return newpage;//this returns the collection
	},
	getPages:function(n)
	{
		return this.get('pages').models;
	},
	showPage:function(n)
	{
		this.getPages()[n].set({displayed:true});
	},
	showPageById:function(id)
	{
		// I need to traverse as deep as I can to find a page with that name
		//var pageObj = this.get('pages').where({el:id});	
		//You can set up the Analytics here
		var pageObj = this.getPageById(id)
		if(pageObj.length!=0){
			pageObj[0].set({displayed:true});
		}
	},
	getPageById:function(id)
	{
		var res = [];
		var currentSearchObj = this
		var findme =  function(obj,id)
		{	
			var fobj = obj.get(this.relations[0].key).where({el:id});	
			if(fobj.length==0)
			{
				for(var i=0;i<obj.get(this.relations[0].key).length;i++)
				{
					if(obj.getPages()[i].get('pageController')!=null)
					{
						currentSearchObj = obj.getPages()[i].get('pageController');
						findme(currentSearchObj,id);
					}
				}
			}
			else
			{
				res.push(fobj[0]);			
			}
		}		
		findme(currentSearchObj,id);
		return res;
	
	}
	
});


WidgetEng.WidgetEngRouter = Backbone.Router.extend({
/* define the route and function maps for this router */

	routes: {
	"home/" : "showHomePage",
	"about1/" : "showAbout1",
	/*Sample usage: http://unicorns.com/#about*/
	//"photos/:id" : "getPhoto",
	/*This is an example of using a ":param" variable which allows us to match any of the components between two URL slashes*/
	/*Sample usage: http://unicorns.com/#photos/5*/
	//"search/:query" : "searchPhotos",
	/*We can also define multiple routes that are bound to the same map function, in this case searchPhotos(). Note below how we're optionally passing in a reference to a page number if one is supplied*/
	/*Sample usage: http://unicorns.com/#search/lolcats*/
	//"search/:query/p:page" : "searchPhotos",
	/*As we can see, URLs may contain as many ":param"s as we wish*/ /*Sample usage: http://unicorns.com/#search/lolcats/p1*/
	//"photos/:id/download/*imagePath" : "downloadPhoto",
	/*This is an example of using a *splat. splats are able to match any number of
	URL components and can be combined with ":param"s*/
	/*Sample usage: http://unicorns.com/#photos/5/download/files/lolcat-car.jpg*/
	/*If you wish to use splats for anything beyond default routing, it's probably a good
	idea to leave them at the end of a URL otherwise you may need to apply regular expression parsing on your fragment*/
	"*other/" : "defaultRoute"
	/*This is a default route that also uses a *splat. Consider the
	default route a wildcard for URLs that are either not matched or where
	the user has incorrectly typed in a route path manually*/ /*Sample usage: http://unicorns.com/#anything*/
	},
	showAbout: function(){WidgetEng.mainController.showPage(0)},
	showAbout1: function(){WidgetEng.mainController.showPage(1)},
	getPhoto: function(id){
		/*
		Note that the id matched in the above route will be passed to this function
		*/
		console.log("You are trying to reach photo " + id); 
	},
	searchPhotos: function(query, page){
		var page_number = page || 1;
		console.log("Page number: " + page_number + " of the results for " + query);
	},
	downloadPhoto: function(id, path){ },
	showHomePage:function(){
		WidgetEng.mainController.showPage(0);
	},
	defaultRoute: function(other){	
		WidgetEng.mainController.showPageById(other)
	}
});

