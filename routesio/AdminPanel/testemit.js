exports.testemit = function(app)
{
	app.io.route('testemit',
		function(req)
		{		
			
		var data = [
			{
				background: "green",
				border: "",
				classname: "page",
				color: "white",
				display: true,
				el: "page4",
				flex_child: false,
				height: "100%",
				html: "testPage",
				objectClass: "Page",
				pageController: null,
				parent: null,
				position: "relative",
				selectable: false,
				tagName: "div",
				viewClass: "PageView",
				width: "100%",
			
			},
			{
				background: "green",
				border: "",
				classname: "page",
				color: "white",
				display: true,
				el: "page3",
				flex_child: false,
				height: "100%",
				html: "testPage",
				objectClass: "Page",
				pageController: null,
				parent: null,
				position: "relative",
				selectable: false,
				tagName: "div",
				viewClass: "PageView",
				width: "100%",
			
			}
		]
		req.io.emit('pages',data);
			
	});
};
