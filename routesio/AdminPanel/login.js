var User = require('../../models/AdminPanel/users');

exports.login = function(app){
	app.io.route('login',function(req){
		var data = req.data;
		console.log('trying to login')
		User.getAuthenticated(data.user, data.pass, function(err, user, reason) {
		        if (err) throw err;

		        // login was successful if we have a user
		        if (user) {
		            // handle login success
		            console.log('login success');
					console.log(user);
					req.session.auth = true;
					req.session.room = data.room;
					req.session.user = user;
					console.log(user);
					req.session.save(function() {
				        req.io.emit('session', req.session)
				    });
		            return;
		        }

		        // otherwise we can determine why we failed
		        var reasons = User.failedLogin;
				console.log(reasons)
		        switch (reason) {
		            case reasons.NOT_FOUND:
		            case reasons.PASSWORD_INCORRECT:
		                // note: these cases are usually treated the same - don't tell
		                // the user *why* the login failed, only that it did
		                break;
		            case reasons.MAX_ATTEMPTS:
		                // send email or otherwise notify user that account is
		                // temporarily locked
		                break;
		        }
		    });
	});
};

