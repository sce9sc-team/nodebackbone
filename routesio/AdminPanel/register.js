var User = require('../../models/AdminPanel/users');

exports.register = function(app){
	return app.io.route('register',function(req){
		var data = req.data;

		var newUser = new User({
		    username: data.user,
		    password: data.pass
		});

		newUser.save(function(err) {
			if (err) throw err;
		    // attempt to authenticate user
		    User.getAuthenticated(User.username, User.password, function(err, user, reason) {
		        if (err) throw err;

		        // login was successful if we have a user
		        if (user) {
		            // handle login success
		            console.log('login success');
		            return;
		        }

		        // otherwise we can determine why we failed
		        var reasons = User.failedLogin;
		        switch (reason) {
		            case reasons.NOT_FOUND:
		            case reasons.PASSWORD_INCORRECT:
		                // note: these cases are usually treated the same - don't tell
		                // the user *why* the login failed, only that it did
		                break;
		            case reasons.MAX_ATTEMPTS:
		                // send email or otherwise notify user that account is
		                // temporarily locked
		                break;
		        }
		    });
		});	
	});
};