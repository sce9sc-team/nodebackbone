var User = require('../../models/AdminPanel/users');

exports.isloggedin = function(app){
	app.io.route('isloggedin', function(req) {
	    if(req.session.auth)
		{
			console.log('logged in');
			req.io.join(req.session.room)
			req.io.room(req.session.room).broadcast('announce', {
			        message: 'New client in the ' + req.session.room + ' room. '
			})
			req.io.emit('session',req.session);
		}
		else
		{
			console.log('not logged in');
		}

	});	
};