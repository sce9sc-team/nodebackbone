var User = require('../../models/AdminPanel/users');

exports.logout = function(app){
	app.io.route('logout',function(req){
		//var data = req.data;
		if(req.session.auth)
		{
			console.log('logout')
			console.log(req.session.cookies)
			req.session.destroy(function() {
		        console.log('session deleted');
				//need to reload page			
			    req.io.emit('logout');
			});
		}
	});
};

