var Site = require('../../models/SiteBuilder/sites');

exports.saveSite = function(app)
{
	app.io.route('saveSite',function(req)
	{
			console.log(req.data);
			var data = req.data;
			
			var newSite = new Site({	
				'title': 'testSite',
				'domain': 'testdomain.com',
				'description': 'bla bla',
				'keywords': 'testsite',
				'data':data,//  Iam not sure
				
			});
			
			
			newSite.save(function(err) {
				if (err) throw err;
			   	console.log('success');
				//var r = [{"el":"h1box","width":"100%","height":"100%","border":"1px solid black","display":true,"background":"blue","flex_child":false,"events":[],"tagName":"div","classname":"vbox","color":"white","position":"relative","widgets":[{"el":"1box","width":"100px","height":"100px","border":"1px solid black","display":true,"background":"red","flex_child":false,"events":[],"tagName":"div","classname":"box","color":"white","position":"relative","widgets":[{"el":"img","width":"100%","height":"100%","border":"1px solid black","display":true,"background":"","flex_child":false,"events":[],"tagName":"img","classname":"image","color":"white","position":"relative","widgets":[],"parent":null,"objectClass":"Image","viewClass":"imageView","selectable":true,"prevEvents":[],"src":"../images/test.png","handles":false,"canResize":false}],"parent":null,"objectClass":"Box","viewClass":"BoxView","selectable":true,"prevEvents":[],"handles":false,"canResize":false},{"el":"pageController1","width":"100%","height":"100%","border":"1px solid black","display":true,"background":"red","flex_child":false,"events":[],"tagName":"div","classname":"pageController","color":"white","position":"relative","widgets":[{"el":"page2","width":"100%","height":"100%","border":"","display":false,"background":"yellow","flex_child":false,"events":[],"tagName":"div","classname":"page","color":"white","position":"relative","widgets":[],"parent":null,"objectClass":"Page","viewClass":"PageView","selectable":false,"prevEvents":[],"pageController":null}],"parent":null,"objectClass":"PageController","viewClass":"PageControllerView","selectable":false,"prevEvents":[],"currentPage":null}],"parent":null,"objectClass":"vBox","viewClass":"vBoxView","selectable":true,"prevEvents":[],"handles":false,"canResize":false}]
				req.io.emit('updateSite',data);
			    
			   });
	});
};
